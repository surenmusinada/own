# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from review.models import Review
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, HttpResponse
from django.core.mail import send_mail, BadHeaderError
import requests
from django.views.generic import View
from django.contrib import messages
from django.http import JsonResponse
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.template.loader import render_to_string
import json
from django import template
from .models import Element, RecentlyViewedElements, FavoriteElements
from .forms import ChangeNameForm, InviteForm
from .forms import UpdateProfileForm
from .forms import ChangeUserEmailForm
from .forms import ProfilePictureForm
from .forms import FeedbackForm
from django.core.serializers import serialize
from django.shortcuts import render, get_object_or_404
from django.contrib.auth import get_user_model
from django.conf import settings
from review.models import RatingCategory
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.sites.shortcuts import get_current_site
from .helpers import get_element_from_cache, is_element_favorite, get_user_recently_rated_elements, \
    get_user_favorite_elements, set_recently_viewed, check_if_place_exists, paginate_place_reviews
from .search import get_search_results, determine_entity_type
from mptt.templatetags.mptt_tags import recursetree

User = get_user_model()
# register the tag library
register = template.Library()


class ElementView(View):
    def get(self, request, **kwargs):
        place_id = kwargs.get("place_id")
        search_type = kwargs.get("search_type")
        review = kwargs.get('reviews')
        categories = RatingCategory.objects.all()

        obj, created = Element.objects.get_or_create(
            place_id=place_id
        )


        # paginate place reviews for infinite scroll loading
        reviews =paginate_place_reviews(request.GET.get('page', 1), obj)

        # if user is authenticated
        if request.user.is_authenticated():
            get_element_details = set_recently_viewed(request.user, place_id)
        # if user anonymous
        else:
            get_element_details = get_element_from_cache(place_id)
        # get element details
        element_details = check_if_place_exists(get_element_details)
        if element_details is not None:
            # determine type of the entity
            entity_type = determine_entity_type(element_details['types'])
            obj.category = entity_type
            obj.save()

            return render(request, 'oprapp/element.html',
                          {'place_id': place_id,
                           'element': obj,
                           'element_details': element_details,
                           'categories': categories,
                           'review': review,
                           'reviews': reviews,
                           'element_is_favorite': is_element_favorite(request, place_id),
                           'type': search_type
                           })
        else:
            return render(request, "oprapp/page_not_exist.html",{})


def index(request):
    """
    View function for home page of site.
    """

    if request.method == 'POST':
        search_type = request.POST.get("type")
        search_results = get_search_results(request.POST.get("Place"), search_type)
        return render(request, 'oprapp/search.html', {'result': search_results, 'type': search_type, })

    # request method is get
    else:

        return render(request, 'oprapp/index.html')


# reviews of elements
def reviewsOfElement(request, place_id):
    reviews = Review.objects.all().filter(object_id=Element.objects.get(place_id=place_id).pk)

    result = "<h1>Place : " + place_id + "</h1><br/><br/>"
    for review in reviews:
        result += str(review.user) + "  :  " + str(review.content) + "  at  " + "<br>"
    return HttpResponse(result)


# load dashboard
class Dashboard(View):
    """ This class loads user's dashboard """

    def get(self, request):

        results_paginate = Paginator(get_user_recently_rated_elements(request), 4)
        favorit_paginate = Paginator(get_user_favorite_elements(request), 4)
        page_rev = request.GET.get('page_rev', 1)
        page_fav = request.GET.get('page_fav', 1)
        ###################
        try:
            reviews = results_paginate.page(page_rev)
            favorites_ = favorit_paginate.page(page_fav)
        except PageNotAnInteger:
            reviews = results_paginate.page(1)
            favorites_ = favorit_paginate.page(1)
        except EmptyPage:
            reviews = results_paginate.page(results_paginate.num_pages)
            favorites_ = favorit_paginate.page(favorit_paginate.num_pages)

        return render(request, "dashboard/dashboard.html",
                      {'reviewed_elements': reviews,
                       'favorite_elements': favorites_})


# change password
class ChangePassword(View):
    ''' This class changes a user's password from dashboard'''

    data = dict()

    def get(self, request):
        form = PasswordChangeForm(request.user)
        html_form = render_to_string('dashboard/includes/partial_change_password.html',
                                     {'form': form},
                                     request=request,
                                     )
        return JsonResponse({'html_form': html_form})

    def post(self, request):
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            self.data['form_is_valid'] = True
            update_session_auth_hash(request, user)  # Update session authentication!
            self.data["updated"] = "Your password was successfully updated!"
        else:
            self.data['form_is_valid'] = False
        # in case form is invalid do this
        context = {'form': form}
        self.data['html_form'] = render_to_string('dashboard/includes/partial_change_password.html',
                                                  context,
                                                  request=request
                                                  )
        return JsonResponse(self.data)


# Delete user account
class DeleteAccount(View):
    """" This class deletes a user's account from dashboard  """

    def get(self, request):
        html_form = render_to_string('dashboard/includes/partial_delete_account.html',
                                     request=request,
                                     )
        return JsonResponse({'html_form': html_form})

    def post(self, request):
        # set email body
        email_body_template = 'registration/account_deletion_text.txt'
        message = render_to_string(email_body_template)
        # get the user
        user_id = int(request.user.pk)
        removable_user = User.objects.get(pk=user_id)
        if removable_user is not None:
            # email the user
            send_mail(
                'One Planet Rating',
                message,
                'oneplanetratingdev@example.com',
                [request.user.email],
                fail_silently=False,
            )
            # delete user profile picture if the user has one
            if removable_user.profile.profile_picture is not None:
                # delete it
                removable_user.profile.profile_picture.delete(save=True)
            # delete the user
            removable_user.delete()
        # redirect to homepage
        return redirect('index')


# Invite to OPR
class InviteFriend(View):
    """" This class deletes a user's account from dashboard  """

    def get(self, request):
        form = InviteForm()
        context = {
            'form': form,
        }
        return render(request, "oprapp/invite.html", context)

    def post(self, request):
        form = InviteForm(request.POST)
        if form.is_valid():
            # email subject
            subject = str(request.user.get_full_name()) + " invites you to" \
                                                          " sustainable travel review platform One Planet Rating."
            # email context
            context = {
                'user': request.user,
                'site': get_current_site(self.request)
            }
            # email content
            html_content = render_to_string('oprapp/invitation_email.html', context)
            to_email = request.POST.get('email')
            # send email
            request.user.invite_friend(subject, html_content, settings.DEFAULT_FROM_EMAIL, to_email)
            return render(request, "oprapp/invitation_success.html")
        return render(request, "oprapp/invite.html", {"form": form})


@login_required
def update_name(request, pk):
    ''' This function changes a user's name from dashboard '''
    data = dict()
    edit_user = get_object_or_404(User, pk=pk)

    if request.method == 'POST':
        # populate fields with instance's data and POST
        form = ChangeNameForm(request.POST, instance=edit_user)
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            data['isName'] = True
            updated_user = User.objects.filter(pk=pk).values()
            data["updated_user"] = list(updated_user)
            data["updated"] = "Your name was successfully updated!"
        else:
            data['form_is_valid'] = False
    else:
        # populate fields with instance's data
        form = ChangeNameForm(instance=edit_user)

    context = {'form': form}
    data['html_form'] = render_to_string('dashboard/includes/partial_change_name.html',
                                         context,
                                         request=request,
                                         )
    return JsonResponse(data)


# update profile details
@login_required
def update_profile(request, pk):
    edit_user = get_object_or_404(User, pk=pk)
    data = dict()
    if request.method == 'POST':
        # populate fields with instance's data and POST
        form = UpdateProfileForm(request.POST, instance=edit_user.profile)
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            data['isProfile'] = True
            updated_profile = edit_user.profile
            # serialize and pass the profile as a list
            data["updated_profile"] = serialize('json', [updated_profile])
            data["updated"] = "Your profile details were successfully updated!"
        else:
            data['form_is_valid'] = False
    else:
        form = UpdateProfileForm(instance=edit_user.profile)
    context = {'form': form}
    data['html_form'] = render_to_string('dashboard/includes/partial_update_profile.html',
                                         context, request=request, )
    return JsonResponse(data)


# upload user profile pic
class UploadProfilePicture(View):
    data = dict()
    template_name = 'dashboard/includes/partial_upload_profile_picture.html'
    uploaded = False

    def post(self, request):
        form = ProfilePictureForm(request.POST, request.FILES, instance=request.user.profile)
        if form.is_valid():
            user = get_object_or_404(User, pk=request.user.pk)
            # if the user has already a profile pic
            if user.profile.profile_picture is not None:
                # delete it
                user.profile.profile_picture.delete(save=True)
            form.save()
            self.data['form_is_valid'] = True
            self.data['uploadAvatar'] = True
            self.uploaded = True

        else:
            self.data['form_is_valid'] = False
            form = ProfilePictureForm()
            self.data['html_form'] = render_to_string(self.template_name,
                                                      {"form": form}, request=request, )

        return redirect(request.META['HTTP_REFERER'])

    def get(self, request):
        form = ProfilePictureForm()
        self.data['html_form'] = render_to_string(self.template_name,
                                                  {"form": form}, request=request, )
        return JsonResponse(self.data)


# delete user profile pic
@login_required
def delete_profile_picture(request):
    data = dict()
    if request.method == "POST":
        request.user.profile.profile_picture.delete(save=True)
        data['form_is_valid'] = True
        data["removeAvatar"] = True

    data['html_form'] = render_to_string('dashboard/includes/partial_profile_pic_delete_confirm.html',
                                         request=request, )
    return JsonResponse(data)


# change email (username)
@login_required
def change_email(request):
    # gonna get back to this shortly
    data = dict()
    if request.method == "POST":
        form = ChangeUserEmailForm(request.user, request.POST)
        if form.is_valid():
            return HttpResponse("form was valid")
            password = form.cleaned_data.get("password")
            user = request.user
            if user.check_password(password):
                user = form.save()
                data['form_is_valid'] = True
                update_session_auth_hash(request, user)  # Update session authentication!
        else:
            return HttpResponse("form was invalid")
            data['form_is_valid'] = False
    else:
        form = ChangeUserEmailForm()

    context = {'form': form}
    data['html_form'] = render_to_string('dashboard/includes/partial_change_email.html',
                                         context, request=request, )
    return JsonResponse(data)


# feedback
@login_required
def feedback(request):
    if request.method == "POST":
        form = FeedbackForm(request.POST)
        if form.is_valid():
            subject = request.POST.get('feedback_subject')
            message = request.POST.get('message')
            user_email = request.user.email
            try:
                send_mail(subject, message, user_email, [settings.EMAIL_HOST_USER])
            except BadHeaderError:
                return HttpResponse('Something went Wrong.')
            return redirect(feedback_confirm)
    else:
        form = FeedbackForm()
    context = {
        'form': form,
    }
    return render(request, "oprapp/feedback.html", context)


@login_required
def feedback_confirm(request):
    return render(request, "oprapp/feedback_confirmation.html", {})


# method  called by Ajax
@login_required
def up_down_vote(request, review_id):
    data = dict()
    vote_type = request.POST.get("vote_type")
    data["vote_type"] = vote_type
    from review.models import Review
    review = Review.objects.get(pk=review_id)

    if vote_type == "like":
        if review.votes.exists(request.user.pk):
            review.votes.delete(request.user.pk)
            data["vote_removed"] = True
        else:
            review.votes.up(request.user.pk)
            data["up_vote_added"] = True

    if vote_type == "dislike":
        down_vote = False
        # if vote already exists
        if review.votes.exists(request.user.pk) or review.votes.exists(request.user.pk, action=1):
            # if up vot already  exists
            if review.votes.exists(request.user.pk, action=0):
                down_vote = True
            review.votes.delete(request.user.pk)
            data["vote_removed"] = True
            if down_vote:
                review.votes.down(request.user.pk)
                data["down_vote_added"] = True
        else:
            review.votes.down(request.user.pk)
            data["down_vote_added"] = True

    # article.refresh_from_db()
    data["num_of_likes"] = review.votes.count(action=0)
    data["num_of_dislikes"] = review.votes.count(action=1)
    data["article_id"] = review_id

    return JsonResponse(data)


# add to favorites
def add_to_favorites(request):
    favorite = request.POST.get("place_id", "")
    obj, created = FavoriteElements.objects.get_or_create(user=request.user, element_id=favorite)
    obj.save()
    return HttpResponse('success', content_type='application')


# remove from favorites
def remove_from_favorites(request):
    element_id = request.POST.get("place_id", "")
    request.user.favorite_element.all().filter(element_id=element_id).delete()
    return HttpResponse('success', content_type='application')
