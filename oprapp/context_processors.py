from .helpers import (get_user_recently_viewed_elements,
                      get_user_favorite_elements,
                      get_user_recently_rated_elements, get_search_categories)


def add_variable_to_context(request):
    user = request.user
    search_type = request.GET.get("search_type") or request.POST.get("search_type")

    if user.is_anonymous():
        return {
            'recently_viewed': "",
            'element_infos': "",
            'user': request.user,
            'favorites': "",
            'type': search_type,
            'search_categories': get_search_categories()
        }

    else:

        return {
            'element_infos': get_user_recently_viewed_elements(request),
            'user': request.user,
            'favorites':   get_user_favorite_elements(request)[:5],
            'type': search_type,
            'rr': get_user_recently_rated_elements(request)[:5],
            'search_categories': get_search_categories()
        }
