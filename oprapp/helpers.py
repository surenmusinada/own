from django.conf import settings
import requests
# import cache
from django.core.cache import cache
from .models import RecentlyViewedElements, SearchCategory
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from review.templatetags.review_tags import get_reviews


def get_element_from_cache(unique_id):
    """
        attempts to get the element from cache
        if the element is not in the cache, calls the google api then adds the
        element to the cache for next time.

    """

    # cache data for 24 hours
    cache_time = 86400  # time in seconds for cache to be valid
    cache_key = unique_id
    # attempt to get data from cache
    data = cache.get(cache_key)
    # get data from google places API
    if data is None:
        # make a request to google api
        data = requests.get(
            'https://maps.googleapis.com/maps/api/place/details/json?placeid=' + unique_id + '&key=' + settings.PLACES_API_KEY)
        # check if the request returned data and its length is greater than 0
        if 'result' in data.json() and len(data.json()) > 0:
            # set cache for next time
            cache.set(cache_key, data, cache_time)

    return data


def is_element_favorite(request, place_id):
    """
    checks if the element is a favorite element
    returns either true or false
    """

    if request.user.is_anonymous():
        return False

    favorites = request.user.favorite_element.all()
    if favorites is not None:
        for f in favorites:
            if f.element_id == place_id:
                return True
    else:
        return False


def get_user_favorite_elements(request):
    """
       returns this user's favorite elements

    """
    user = request.user
    # favorites
    favorites = user.favorite_element.all().order_by('-date_time')
    result = []

    if favorites is not None:

        for f in favorites:
            data = get_element_from_cache(f.element_id)
            result.append([f, data.json()['result']])
    return result


def get_user_recently_rated_elements(request):
    """
           returns this user's recently reviewed/rated elements

    """
    reviewed_elements = []
    result = []
    # get all user rated items
    for review in request.user.reviews.all().order_by('-creation_date'):
        if str(review.reviewed_item) not in reviewed_elements:
            reviewed_elements.append(str(review.reviewed_item))
    for f in reviewed_elements:
        data = get_element_from_cache(f)
        result.append([f, data.json()['result']])

    return result


def get_user_recently_viewed_elements(request):
    """
           returns this user's recently viewed elements

    """
    user = request.user
    # recently viewed
    recently_viewed_elements = user.recently_viewed_elements.all().order_by('-date_time')[:5]
    result = []

    if recently_viewed_elements is not None:
        for recently_viewed in recently_viewed_elements:
            data = get_element_from_cache(recently_viewed.element_id)
            result.append([recently_viewed, data.json()['result']])

    return result


# trigger function for saving recently viewed items
def set_recently_viewed(user, place_id):
    # first, retrieve the recently user viewed elements
    user_viewed_objects = user.recently_viewed_elements.all().order_by('-date_time')
    # although we get_or_create does not let duplicates, still it is saver to check
    repeat_element = None

    # check it this is a real place_id
    element_data = get_element_from_cache(place_id)
    if 'result' in element_data.json() and len(element_data.json()) > 0:
        if len(user_viewed_objects) < 6 or user_viewed_objects is None:
            obj, created = RecentlyViewedElements.objects.get_or_create(user=user, element_id=place_id)
            obj.save()
        else:
            last = user_viewed_objects[len(user_viewed_objects) - 1]
            RecentlyViewedElements.objects.filter(id=last.id).delete()
            obj, created = RecentlyViewedElements.objects.get_or_create(user=user, element_id=place_id)
            obj.save()

        return element_data

    return False


def get_search_categories():
    # cache data for 24 hours
    cache_time = 86400  # time in seconds for cache to be valid
    cache_key = "search_categories"
    # attempt to get data from cache
    # search_categories = cache.get(cache_key)

    # if search_categories is None:
    search_categories = SearchCategory.objects.all().filter(parent=None)

    # for search_category in search_categories:


    # children = SearchCategory.objects.all().filter(parent= search_category)
    # print(children)
    # set for next use
    # if search_categories is not None:
    # cache.set(cache_key, search_categories, cache_time)

    return search_categories


def store_element_in_cache(place_id, element_details):
    if cache.get(place_id) is not None:
        print(str(place_id) + "Already in cache")
        return False

    cache.set(place_id, element_details, 86400)
    print(str(place_id) + " Element stored in cache")


def get_month_name(num):
    months = ["January", "February", "March", "April", "May", "June",
              "July", "August", "September", "October", "November",
              "December"]

    if num is None:
        return False
    else:
        for index, month in enumerate(months, start=1):
            if num == index:
                return month


def check_if_place_exists(get_element_details):
    if get_element_details is False:
        return None

    if 'result' in get_element_details.json():
        element_details = get_element_details.json()['result']
    else:
        element_details = None

    return element_details


def paginate_place_reviews(page_number, obj):
    reviews_paginator = Paginator(get_reviews(obj), 1)
    try:
        reviews = reviews_paginator.page(page_number)
    except PageNotAnInteger:
        reviews = reviews_paginator.page(1)
    except EmptyPage:
        reviews = reviews_paginator.page(reviews_paginator.num_pages)
    return reviews
