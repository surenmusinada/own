from django import forms
from django.core.validators import RegexValidator
from registration.forms import RegistrationForm
from django.conf import settings
from cuser .models import CUser
from .models import Profile, PromotionCodes
from django.contrib.auth import get_user_model
from review.models import Review, Rating, RatingCategory
from django_libs.loaders import load_member
from django.utils.translation import get_language
User = get_user_model()


# Places form
class PlaceSearchForm(forms.Form):
    Place = forms.CharField(label='Place', max_length=100)


class ReviewForm(forms.ModelForm):

    def __init__(self, reviewed_item, user=None, *args, **kwargs):
        self.user = user
        self.reviewed_item = reviewed_item
        self.widget = load_member(getattr(settings, 'REVIEW_FORM_CHOICE_WIDGET', 'django.forms.widgets.Select'))()
        super(ReviewForm, self).__init__(*args, **kwargs)
        # Dynamically add fields for each rating category
        for category in RatingCategory.objects.all():
            field_name = 'category_{0}'.format(category.pk)
            choices = category.get_choices()
            # self.fields[field_name] = forms.ChoiceField(choices=choices, label=category.name, help_text=category.question, widget=self.widget, )
            self.fields[field_name] = forms.ChoiceField(choices=choices, label=category.name, help_text=category.question, widget=forms.widgets.CheckboxSelectMultiple(attrs={'class':'rating-input'}), )
            # self.fields[field_name].required = category.required
            self.fields[field_name].required = False
            if self.instance.pk:
                try:
                    self.initial.update({'category_{0}'.format(category.pk): Rating.objects.get(review=self.instance, category=category).value, })
                except Rating.DoesNotExist:
                    pass

    def save(self, *args, **kwargs):
        if not self.instance.pk:
            self.instance.user = self.user
            self.instance.reviewed_item = self.reviewed_item
            self.instance.language = get_language()
        self.instance = super(ReviewForm, self).save(*args, **kwargs)
        # Update or create ratings
        for field in self.fields:
            if field.startswith('category_'):
                rating, created = Rating.objects.get_or_create(review=self.instance, category=RatingCategory.objects.get(pk=field.replace('category_', '')), )
                rating.value = self.cleaned_data[field]
                rating.save()

        self.instance.average_rating = self.instance.get_average_rating()
        self.instance.save()

        return self.instance
    class Meta:
        model = Review
        fields = ('content',)



class CustomReviewForm(ReviewForm):
    """Form to make use of the review extra info."""

    def __init__(self, reviewed_item, user=None, *args, **kwargs):
        super(CustomReviewForm, self).__init__(reviewed_item, user, *args, **kwargs)
        # choices = [(x.pk, x.name) for x in WeatherCondition.objects.all()]
        # self.fields['weather_conditions'] = forms.fields.ChoiceField(choices=choices)

    def save(self, *args, **kwargs):
        self.instance = super(CustomReviewForm, self).save(*args, **kwargs)
        # ReviewExtraInfo.objects.create(type='weather_conditions', review=self.instance, content_object=WeatherCondition.objects.get(pk=self.cleaned_data['weather_conditions']), )
        return self.instance


# force unique emails
def force_unique_email(value):

    email = value
    email_qs = User.objects.filter(email=email)
    if email_qs.exists():
        raise forms.ValidationError("This email is already registered, Please try a different email address")

def check_promo_code(value):
    promo_code = value
    try:
        codes=PromotionCodes.objects.get(promo_code=promo_code)
    except PromotionCodes.DoesNotExist:
        codes=None

    if codes is None:
        raise forms.ValidationError("This promotion code is already registered or does not exist, Please try a different promotion code")
    else:
        codes.save()

# User signup form
class SignUpForm(RegistrationForm):
    # makes the field a password field.

    GENDER_CHOICES = (
        ('','Select your gender'),
        ('Male', 'Male'),
        ('Female', 'Female'),
	('Other', 'Other'),
 
    )

    city = forms.CharField(label= "City")
    date_of_birth = forms.DateField(widget=forms.DateInput(format='%Y-%m-%d'),
    input_formats=('%Y-%m-%d',))

    gender = forms.ChoiceField(choices=GENDER_CHOICES,
                               widget=forms.Select(attrs={
                                   "id": "gender",
                                   "tabindex": "1",
                               }), required=True)

    # using the username as email
    email = forms.EmailField(label="Email Address", validators=[force_unique_email])
    password1= forms.CharField(widget=forms.PasswordInput, min_length=8, label ="Password")
    password2 = forms.CharField(widget=forms.PasswordInput, min_length=8, label="Retype Password")
    first_name = forms.CharField(label=" First and last name")
    promo_code = forms.CharField(label="Promotion code", validators=[check_promo_code])

    class Meta:
        model = CUser
        fields = ["first_name","email","password1","password2"]


#  change name (dashboard)
class ChangeNameForm (forms.ModelForm):
    first_name = forms.CharField(min_length=2, required=True)
    last_name = forms.CharField(required=False)

    class Meta:
        model = CUser
        fields = ["first_name", "last_name"]


# update user profile (dashboard)
class UpdateProfileForm (forms.ModelForm):
    GENDER_CHOICES = (
        ('', 'Select your gender'),
        ('Male', 'Male'),
        ('Female', 'Female'),
	    ('Other', 'Other'),
    )

    city = forms.CharField(label="city", required=True)
    birth_date = forms.DateField(widget=forms.DateInput(format='%Y-%m-%d', attrs={
       "id":"id_date_of_birth",
    }),input_formats=('%Y-%m-%d',), required=True, label="Date of birth")

    gender = forms.ChoiceField(choices=GENDER_CHOICES,
                               widget=forms.Select(attrs={
                                   "id": "gender",
                                   "tabindex": "1",
                               }), required=True)

    about_me = forms.CharField(widget=forms.Textarea(attrs={
        "rows":"5",
    }), required=False, label="Write something about yourself")

    class Meta:
        model = Profile
        fields = ["about_me","address","city","phone_number","birth_date","gender"]


# change user email (dashboard)
class ChangeUserEmailForm(forms.ModelForm):
    email = forms.EmailField(required=True,   label= "new email")
    password = forms.CharField(widget=forms.PasswordInput, min_length=8, label="Enter your password")

    class Meta:
        model = User
        fields = ["email","password"]

    def clean(self):
        email = self.cleaned_data.get("email")
        email_qs = User.objects.filter(email=email)
        if email_qs.exists():
            raise forms.ValidationError("This email is already registered, Please try a different email address")
        return email


# upload profile picture (dashboard)
class ProfilePictureForm(forms.ModelForm):
        profile_picture = forms.ImageField(required= True)

        class Meta:
            model = Profile
            fields = ["profile_picture",]


class FeedbackForm(forms.Form):
    features_list = (
        ('', 'Please select a subject'),
        ('login', 'login'),
        ('signup', 'signup'),
        ('search', 'search'),
        ('ratings', 'rating'),
        ('reviews', 'reviews'),
        ('dashboard', 'dashboard'),
        ('comments', 'comments'),
        ('design', 'design'),
        ('other', 'other'),
    )
    feedback_subject = forms.ChoiceField(choices=features_list,
                               widget=forms.Select(attrs={
                               }), required=True)
    message = forms.CharField(widget=forms.Textarea(attrs={
        "rows": "5",
        "placeholder": "Please include a detailed description of your idea or suggestion"
    }), required=True, label="Your Feedback", min_length=3)


class InviteForm(forms.Form):
    email = forms.EmailField(label="Email Address", required=True)

    def clean(self):
        cleaned_data = super(InviteForm, self).clean()
        email = cleaned_data.get("email")
        email_qs = User.objects.filter(email=email)
        if email_qs.exists():
            raise forms.ValidationError(
                "The contact you entered is already a member,"
                " please try another email address."
            )
