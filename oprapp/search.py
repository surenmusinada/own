from oneplanet import settings
from django.core.cache import cache
import requests

from oprapp.helpers import store_element_in_cache
from oprapp.models import SearchCategory


def get_search_results(place, search_type):
    cache_time = 20  # time in seconds for cache to be valid
    cache_key_actual = place.replace(" ", "")
    cache_key_related = cache_key_actual + search_type.replace(" ", "")
    actual_place = cache.get(cache_key_actual)
    search_results = cache.get(cache_key_related)
    # over all search results

    if actual_place is None:
        actual_place = requests.get('https://maps.googleapis.com/maps/api/place/textsearch/json?query='+place+'&key='+settings.PLACES_API_KEY)
        # set for next time
        cache.set(cache_key_actual, actual_place, cache_time)

    if search_results is None:
        search_results = []

        # add actual type if it is not None, a city and cities categories is selected
        if check_if_term_is_city(actual_place) == True and search_type == "cities":
            search_results.append(actual_place.json()['results'])

        # check if term is a hotel
        if check_if_term_is_hotel(actual_place) == True and search_type == "hotels":
            search_results.append(actual_place.json()['results'])
        # check if term is a restaurant
        if check_if_term_is_restaurant(actual_place) == True and search_type == "restaurants":
            search_results.append(actual_place.json()['results'])

        search_results.append(requests.get(
            'https://maps.googleapis.com/maps/api/place/textsearch/json?query='+search_type+' near '+place+'&key='+settings.PLACES_API_KEY).json()['results'])
        # set for next time
        cache.set(cache_key_related, search_results, cache_time)
    return search_results


def check_if_term_is_city(term):
    try:
        term_types = term.json()['results'][0]["types"]
    except IndexError:
        return None

    if "political" or "locality" in term_types:
        return True
    return False


def check_if_term_is_hotel(term):
    try:
        term_types = term.json()['results'][0]["types"]

    except IndexError:
        return None

    if "lodging" in term_types:
        return True
    else:
        return False


def check_if_term_is_restaurant(term):
    try:
        term_types = term.json()['results'][0]["types"]

    except IndexError:
        return None

    if "restaurant" in term_types and "lodging" not in term_types:
        return True
    else:
        return False


def check_if_term_is_activities_sights(term):
    try:
        term_types = term.json()['results'][0]["types"]
    except IndexError:
        return None

    if "point_of_interest" in term_types and "lodging" or "restaurant" not in term_types:
        return True
    else:
        return False


def set_accuracy_term(actual_place):
    if check_if_term_is_city(actual_place) is True:
        accuracy = "in"
    else:
        accuracy = "near"
    return accuracy


def get_search_terms(search_type):
    """
    get search terms dynamically for each search category
    """
    if search_type is None:
        return None

    search_category = SearchCategory.objects.get(name=search_type)
    return list(SearchCategory.objects.all().filter(parent=search_category).values_list('name', flat=True))


# determine the entity type
def determine_entity_type(types):
    if "lodging" in types and "establishment" in types:
        return "hotels"
    elif (
        ("restaurant" in types or "food" in types or 'bakery' in types or 'cafe' in types) and "lodging" not in types):
        return "restaurants"
    elif "locality" in types or "political" in types:
        return "cities"
    elif ("museum" in types or "place_of_worship" in types) and 'point_of_interest' in types:
        return "sights"
    elif(("park" in types or "amusement_park" in types or "travel_agency" in types or
          "point_of_interest" in types) and ("lodging" not in types or
                                           "restaurant" not in types or
                                            "museum" not in types or
                                            "place_of_worship" not in types
                                             )):
        return "activities"
    else:
        return "uncategorized"