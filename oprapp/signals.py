'''


from django.db.models.signals import post_save
from django.dispatch import receiver
from registration.signals import user_registered
from django.contrib.auth.models import User
from .models import Profile


# run this function when user_registered
@receiver(user_registered, sender=User)
def update_user_profile(sender, instance, request,**kwargs):
    # if user is created, then create a user profile for him
       Profile.objects.create(user=instance)
       instance.profile.city =  request.POST.get("birth_date")
       instance.profile.birth_date = request.POST.get("birth_date")
       instance.profile.gender =   request.POST.get("birth_date")
       instance.profile.save();


'''