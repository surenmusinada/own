# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin

from .models import (Element, Profile, SearchCategory, PromotionCodes)
# Register your models here.
admin.site.register(Profile)
admin.site.register(Element)
admin.site.register(SearchCategory)
admin.site.register(PromotionCodes)

