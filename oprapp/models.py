from __future__ import unicode_literals
from django.db import models
# from django.contrib.auth.models import User
from django.conf import settings
from mptt.models import MPTTModel, TreeForeignKey
from rest_auth.rest_registration.signals import user_registered_api
from registration.signals import user_registered


class Element(models.Model):
    place_id = models.CharField(max_length=100, unique=True)
    category = models.CharField(max_length=100, null=False, default="uncategorized")

    def __str__(self):
        return self.place_id
        # return '%s - %s' % (self.place_id, self.id)


class MembershipLevel(models.Model):
    name = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return self.name


# User Profile created after user signup
class Profile(models.Model):
    from cuser.models import CUser
    user = models.OneToOneField(CUser, on_delete=models.CASCADE, related_name="profile")
    about_me = models.TextField(max_length=500, blank=True)
    address = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=255, blank=True)
    phone_number = models.CharField(max_length=15, blank=True)
    # membership_level = models.ForeignKey(MembershipLevel, default=1, blank=True)
    birth_date = models.DateField(blank=True, null=True)
    gender = models.CharField(max_length=20, blank=True, default="")
    profile_picture = models.ImageField(upload_to='avatars/', max_length=255, blank=True)
    promo_code = models.CharField(max_length=50, null=True)

    def __str__(self):
        return self.user.email


class RecentlyViewedElements(models.Model):
    user = models.ForeignKey(
        getattr(settings, 'AUTH_USER_MODEL', 'auth.User'),
        verbose_name=('User'),
        related_name='recently_viewed_elements',
        blank=True, null=True,
    )
    element_id = models.CharField(max_length=100, null=False)
    date_time = models.DateTimeField(auto_now_add=True, null=False)

    def __str__(self):
        return self.element_id


class FavoriteElements(models.Model):
    user = models.ForeignKey(
        getattr(settings, 'AUTH_USER_MODEL', 'auth.User'),
        verbose_name=('User'),
        related_name='favorite_element',
        blank=True, null=True,
    )
    element_id = models.CharField(max_length=100, null=False)
    date_time = models.DateTimeField(auto_now_add=True, null=False)


class SearchCategory(MPTTModel):
    name = models.CharField(max_length=256, null=False)
    icon = models.CharField(max_length=100, null=True, blank=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='search_terms', db_index=True)
    lft = models.PositiveIntegerField(db_index=True, editable=False, null=True, blank=True, )
    rght = models.PositiveIntegerField(db_index=True, editable=False, null=True, blank=True, )

    def __str__(self):
        return self.name



# signals will stay here atleast for now


# run this function when user first registers
def create_user_profile(sender, user, request, **kwargs):
    # if user is created, then create a user profile for him
    Profile.objects.create(user=user)
    fullname = request.POST.get("first_name")
    # get users first name and last name
    complete_name = get_user_first_last_name(fullname)
    user.first_name = complete_name.get("first_name")
    user.last_name = complete_name.get("last_name", "")
    user.profile.city = request.POST.get("city")
    user.profile.birth_date = request.POST.get("date_of_birth")
    user.profile.gender = request.POST.get("gender")
    user.profile.promo_code = request.POST.get("promo_code")
    user.save()
    user.profile.save()


# normal user registration
user_registered.connect(create_user_profile)


# run this function when user first registers
def create_user_profile_api(sender, user, data, **kwargs):
    Profile.objects.create(user=user)
    fullname = data.get("fullname")
    # get users first name and last name
    complete_name = get_user_first_last_name(fullname)
    user.first_name = complete_name.get("first_name")
    user.last_name = complete_name.get("last_name", "")
    user.profile.city = data.get("city")
    user.profile.birth_date = data.get("date_of_birth")
    user.profile.gender = data.get("gender")
    user.profile.promo_code = data.get("promotion_code")
    user.save()
    user.profile.save()


# api registration
user_registered_api.connect(create_user_profile_api)


# get user first name and last name
def get_user_first_last_name(fullname):
    complete_name = {}
    fullname = fullname.split(" ")
    complete_name["first_name"] = fullname[0]

    if len(fullname) > 1:
        last_name = fullname[:0] + fullname[0 + 1:]
        complete_name["last_name"] = " ".join(last_name)

    return complete_name

class PromotionCodes(models.Model):
    promo_code = models.CharField(max_length=100, unique=True)
    is_used = models.BooleanField(null=False, default=False)

    def __str__(self):
        return self.promo_code
