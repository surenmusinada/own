import datetime
import review
from django import template
import requests

from oprapp.helpers import get_month_name
from review.templatetags.review_tags import get_reviews
from django.forms.widgets import Select
from django.conf import settings
from vote.models import UP

from oprapp.models import Element, SearchCategory
from review.models import RatingCategory, RatingCategoryDescriptions
from django.core.exceptions import ObjectDoesNotExist

register = template.Library()


@register.filter('input_type')
def input_type(input):
    return input.field.widget.__class__.__name__

@register.filter(name='zip')
def zip_lists(a, b):
    return zip(a, b)

@register.filter('is_first_level')
def is_first_level(field):
    if field.parent.id is None:
        return False
    else:
        return True


@register.simple_tag
def get_ref(photo_reference):
    return str(photo_reference)


@register.simple_tag
def get_photo(photo_reference):
    print("getting photo")
    return requests.get('https://maps.googleapis.com/maps/api/place/photo?maxwidth=100&photoreference=' + photo_reference + '&key=AIzaSyAwAaru_f4-6PI4mgdqTHJ1Um985NOEYjE').request.url


@register.simple_tag
def get_form(content_type, object_id):
    return review.views.ReviewCreateView.as_view()(content_type=content_type, object_id=object_id);


@register.simple_tag
def get_element(place_id):

    obj, created = Element.objects.get_or_create(
        place_id=place_id)
    return obj


@register.simple_tag
def review_format(score):

    score = "{:.1f} ".format(score)
    int_score = int(float(score))

    full_globes = []
    empty_globes = []
    for i in range(0, int_score):
        full_globes.append("")
    for i in range(int_score, 5):
        empty_globes.append("")

        data = {'score': score, 'empty_globes': empty_globes, 'full_globes': full_globes}
    return {'score': score, 'empty_globes': empty_globes, 'full_globes': full_globes};

@register.assignment_tag
def get_review_average(obj):
    """Returns the review average for an object."""
    total = 0
    if (obj is None):
        return False
    from review.templatetags.review_tags import get_reviews
    reviews = get_reviews(obj)
    if not reviews:
        return False
    for review in reviews:
        average = review.get_average_rating()
        if average:
            total += review.get_average_rating()
    if total > 0:
        return total / reviews.count()
    return False

@register.simple_tag
def get_element_review_category_mode(element, categoryName):
    """Returns the category mode for an object's rating categories ."""
    rating_values_list = []

    # returns false if element is None
    if element is None:
        return False
    # get all the reviews of the element
    from review.templatetags.review_tags import get_reviews
    reviews = get_reviews(element)

    # loop over all the reviews
    for review in reviews:

        for rating in review.ratings.all():
            # only add the values of the passed category
            if rating.category.name == categoryName:
                # if the rating is note None
                rating_value = rating.get_rating_value()
                if rating_value is not None:
                    rating_values_list.append(rating_value)

    return get_list_mode(rating_values_list)


def get_list_mode(value_list):
    import statistics

    # if list is empty

    if not value_list:
        return 0

    try:
        # attempt to get list mode
        mode_value = statistics.mode(value_list)
        return mode_value

    except:
        # if the list has no mode, instead calculate its average
        average_value = get_list_average(value_list)
        return average_value


def get_list_max(value_list):
    max=0
    for value in value_list:
        if value>max:
            max = value
    return max


def get_list_min(value_list):
    min = 0
    for value in value_list:
        if value < min:
            min = value

    return min

def get_list_average(value_list):
    total = 0
    for i in value_list:
        total = total + i
    return total/ len(value_list)

@register.assignment_tag
def get_reviews_number(obj):
    """Returns the number of reviews for an object."""

    if obj is None:
        return 0
    from review.templatetags.review_tags import get_reviews
    reviews = get_reviews(obj)
    if not reviews:
        return 0
    return reviews.count()


@register.simple_tag
def get_ratings_breakdown(element):
    #from operator import itemgetter
    # get all the reviews of the passed element
    from review.templatetags.review_tags import get_reviews
    reviews = get_reviews(element)

    all_ratings_counter = 0
    poor_counter = 0
    fair_counter = 0
    average_counter = 0
    good_counter = 0
    excellent_counter = 0

    for review in reviews:

        for rating in review.ratings.all():
            rating_value = rating.get_rating_value()
            if rating.category.name == "General" and rating_value is not None:
                all_ratings_counter+=1

                if rating_value  == 1:
                    poor_counter += 1
                elif rating_value == 2:
                    fair_counter += 1
                elif rating_value == 3:
                    average_counter += 1
                elif rating_value == 4:
                    good_counter += 1
                elif rating_value == 5:
                    excellent_counter += 1

    if all_ratings_counter ==0 :
        return [("Poor", 0, 0),
                ("Fair", 0, 0),
                ("Average", 0, 0),
                ("Good",0, 0),
                ("Excellent", 0, 0),
                ]
    else:

        data = [

            ("Excellent", excellent_counter, "{0:.0f}".format(float(excellent_counter) / all_ratings_counter * 100)),
            ("Good", good_counter, "{0:.0f}".format(float(good_counter) / all_ratings_counter * 100)),
            ("Average", average_counter, "{0:.0f}".format(float(average_counter) / all_ratings_counter * 100)),
            ("Fair", fair_counter ,"{0:.0f}".format(float(fair_counter)/all_ratings_counter*100)),
            ("Poor", poor_counter, "{0:.0f}".format(float(poor_counter) / all_ratings_counter * 100)),
        ]

        #data = sorted(data,key=itemgetter(1))
        return data


@register.assignment_tag
def get_site_url():
    """Returns the websites url."""
    return settings.SITE_URL


@register.simple_tag
def vote_exists(model, user, action=UP):
    """Returns whether a user has voted."""
    return model.votes.exists(user.pk, action=action)


@register.simple_tag
def get_individual_review_ratings(review, node_name):
    """Returns the rated categories of any individual review."""

    # returns false if review is None
    if review is None:
        return False

    # check if this review as any ratings
    if review.ratings.all():
        # loop over all the ratings for this review
        for rating in review.ratings.all():
            if rating.get_rating_value is not None and rating.category.name != "General":
                if rating.category.name == node_name:
                    return rating.get_rating_value
                else:
                    continue
    else:
        return False

@register.simple_tag
def get_general_rating_category_description(rating):

    if rating == 1:
        return "Poor"
    elif rating == 2:
        return "Fair"
    elif rating == 3:
        return "Average"
    elif rating == 4:
        return "Good"
    elif rating == 5:
        return "Excellent"


@register.simple_tag
def get_rating_cat_desc(raw_id, element):
    if type(raw_id) is int:
        cat_int_id = raw_id
    else:
        if "_" in raw_id:
            cat_id = raw_id.split("_")
            cat_int_id= int (cat_id[1])

    category = RatingCategory.objects.get(pk=cat_int_id)
    try:
        element_category = SearchCategory.objects.get(name=element.category)
        description_obj = RatingCategoryDescriptions.objects.get(rating_category=category,
                                                                 element_category=element_category)
    except ObjectDoesNotExist:
        # return the default text
        return category.question
        # return the personalized text
    return description_obj.help_text


@register.simple_tag
def get_unique_user_rated_elements(user):

    reviewed_elements = []
    # get all user rated items
    for review in user.reviews.all():
        reviewed_elements.append(review.reviewed_item)
    return set(reviewed_elements) # return a unique set


@register.simple_tag
def get_all_rating_categories():
    categories = RatingCategory.objects.all()
    return categories


@register.simple_tag
def validate_rated_cat_value(value):

    if type(value) is int:
        return value
    else:
        return 0


@register.simple_tag
def format_date_time(raw_date):

    new_date = []

    if raw_date is not None:

        new_date.append(raw_date.day)
        new_date.append(get_month_name(raw_date.month))
        new_date.append(raw_date.year)

        return new_date

    return raw_date


@register.simple_tag
def get_tailored_type_text(review_type):

    review_type = review_type.lower()

    if review_type is None:
        return False
    else:
        if review_type == "solo":
            return "traveled"
        elif review_type == "friends" or review_type == "family":
            return "with"
        elif review_type == "couple":
            return "traveled as a"
        elif review_type == "business":
            return " traveled on"


@register.simple_tag
def get_icon_value(icon_value):

    if icon_value is None or "":
        return False

    return str(icon_value)
