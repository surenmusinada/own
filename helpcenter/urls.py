

from django.conf.urls import url , include
from oprapp import views
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.conf.urls.static import static
from  . import views

urlpatterns = [

    url(r'^$', views.HelpCenterIndex.as_view(), name='help_center'),
    url(r'^category/(?P<pk>\d+)$',views.HelpCenterDetail.as_view(), name = "help_center_detail"),
]

# development environment
if settings.DEBUG:
    # upload image
    urlpatterns+= static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns+= static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

