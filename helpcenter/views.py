# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views import generic
from .models import HelpCategory
from django.shortcuts import render
from .models import Legal
# Create your views here.

class HelpCenterIndex(generic.ListView):
    template_name = "helpcenter/help_center.html"
    # pass data as albums_list
    context_object_name = "help_categories"
    def get_queryset(self):
        return list(reversed(HelpCategory.objects.all()))

# DetailView Class
class HelpCenterDetail(generic.DetailView):
    model = HelpCategory
    template_name = "helpcenter/help_center_detail.html"

def terms_of_service(request):
    objects = Legal.objects.all()
    return render(request, "helpcenter/legal.html",{"legal":objects[0]})

def privacy_policy(request):
    objects = Legal.objects.all()
    return render(request, "helpcenter/legal.html",{"legal":objects[1]})