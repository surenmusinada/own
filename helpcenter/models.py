# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.urlresolvers import reverse
from ckeditor.fields import RichTextField

# Create your models here.

class HelpCategory(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("help_center_detail", kwargs={"pk": self.pk})


class HelpArticle(models.Model):
    # one category has many articles
    category = models.ForeignKey(HelpCategory, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    description = models.TextField()
    content = RichTextField()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("help_center_detail", kwargs={"pk": self.category.pk})


class Legal(models.Model):

    title = models.CharField(max_length=255)
    content = RichTextField()
    
    def __str__(self):
        return self.title
