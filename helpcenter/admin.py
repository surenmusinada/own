# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import HelpCategory, HelpArticle, Legal
admin.site.register(HelpCategory)
admin.site.register(HelpArticle)
admin.site.register(Legal)
# Register your models here.
