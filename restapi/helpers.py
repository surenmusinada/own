from oprapp.models import Profile
from oprapp.templatetags.tags import (vote_exists,
                                      get_element,
                                      get_element_review_category_mode,
                                      get_ratings_breakdown)
from review.models import RatingCategory
from review.templatetags.review_tags import get_reviews
from oprapp.helpers import get_element_from_cache

def set_up_review(request, review):

    # ratings array
    ratings = {}

    ratings_tree = []
    categories = RatingCategory.objects.all().exclude(name="General")
    for cat in categories:

        if cat.parent is None:
            sub_categories = RatingCategory.objects.all().filter(parent=cat)

            ratings_tree.append({"name": cat.name,
                                 "description": cat.question,
                                 "rating": get_review_category_rating(review, cat.name),
                                 "subCategories": [{"name": sub_cat.name,
                                                    "description": sub_cat.question,
                                                    "rating": get_review_category_rating(review, sub_cat.name),
                                                    } for sub_cat in sub_categories],
                                 })

    general_cat = RatingCategory.objects.get(name="General")
    ratings["mainCategory"] = {"name": general_cat.name,
                               "description": general_cat.question,
                               "rating": get_review_category_rating(review, general_cat.name),
                               }
    ratings["categories"] = ratings_tree
    details = set_up_review_details(request, review, ratings)
    return details


def set_up_minimal_review(request, review):
    # ratings array
    ratings = {}

    general_cat = RatingCategory.objects.get(name="General")
    ratings["mainCategory"] = {"name": general_cat.name,
                               "description": general_cat.question,
                               "rating": get_review_category_rating(review, general_cat.name),
                               }
    details = set_up_review_details(request, review, ratings)
    return details


def set_up_minimal_review_user(request, review):
    # ratings array
    ratings = {}

    general_cat = RatingCategory.objects.get(name="General")
    ratings["mainCategory"] = {"name": general_cat.name,
                               "description": general_cat.question,
                               "rating": get_review_category_rating(review, general_cat.name),
                               }
    details = set_up_review_details(request, review, ratings)
    details["place_id"] = review.reviewed_item.place_id
    details["user"]["avatar"] = ""

    # append the photo if there was any photos
    place_details = get_element_from_cache(review.reviewed_item).json()
    if "photos" in place_details['result']:
            details["user"]["avatar"] = "https://maps.googleapis.com/maps/api/place/photo?maxheight=200&maxwidht=180&photoreference=" + str(
                (place_details['result']['photos'][0]['photo_reference'])) + "&key=AIzaSyDzCcuh5e2z9diD7o6jH0euGPVS-9GV6xk"

    return details



def set_up_review_details(request, review, ratings):

    # appending a review object
    has_voted_up = vote_exists(review, request.user, 0)
    has_voted_down = vote_exists(review, request.user, 1)

    details = {"review_text": review.content,
               "review_id": review.pk,
               "review_title": review.title,
               "date_of_review": {"year": review.creation_date.year,
                                  "month": review.creation_date.month,
                                  "day": review.creation_date.day,
                                  "hour": review.creation_date.hour,
                                  "minute": review.creation_date.minute,
                                  },

               "categories_tree": ratings,

               "user": {"fullname": review.user.get_full_name(),
                        "avatar": validate_profile_avatar_link(review),
                        "type_of_travel": review.type
                        },
               "votes": {"user_has_liked": has_voted_up, "user_has_disliked": has_voted_down,
                         "likes": review.votes.count(action=0), "dislikes": review.votes.count(action=1)}
               }

    if review.when is not None:
        details["when"] = {"year": review.when.year,
                           "month": review.when.month,
                           "day": review.when.day,
                           }
    else:
        details["when"] = None

    return details


def validate_profile_avatar_link(review):
    profile_avatar_link = ""
    try:
        if review.user.profile is not None:
            profile_avatar_link = review.user.profile.profile_picture.url
    except ValueError:
        profile_avatar_link = ""
    except Profile.DoesNotExist:
        profile_avatar_link = ""
    return profile_avatar_link


def get_review_category_rating(review, node_name):
    """Returns the rated categories of any individual review."""

    value = 0

    # check if this review as any ratings
    if review.ratings.all():
        # loop over all the ratings for this review
        for rating in review.ratings.all():
            if rating.category.name == node_name:
                value = rating.get_rating_value()
            else:
                continue
    return value


def render_minimal_place_details(place_details):

    element = get_element(place_details["place_id"])

    details = {"place_id": place_details["place_id"],
               "image": "",
               "name": place_details["name"],
               "location": {"longitude": place_details["geometry"]["location"]["lng"],
                            "latitude": place_details["geometry"]["location"]["lat"]
                            }}
    # append the photo if there was any photos
    if "photos" in place_details:
        details["image"] = "https://maps.googleapis.com/maps/api/place/photo?maxheight=200&maxwidht=180&photoreference=" + str(
            (place_details['photos'][0]['photo_reference'])) + "&key=AIzaSyDzCcuh5e2z9diD7o6jH0euGPVS-9GV6xk"

    details["most_common_rating"] = get_element_review_category_mode(element,"General")
    details["ratings_count"] = get_reviews(get_element(place_details["place_id"])).count()
    details["ratings_breakdown"] = get_ratings_breakdown(element)
    details["description"] = {}
    details["description"]["address"] = place_details["formatted_address"]
    return details
