import requests
import json
from django.http import HttpResponse
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from rest_framework.views import APIView
from rest_framework.generics import (CreateAPIView,
                                     GenericAPIView,
                                     RetrieveUpdateAPIView,
                                     RetrieveUpdateDestroyAPIView,
                                     DestroyAPIView)

from oprapp.search import get_search_results
from restapi.helpers import (set_up_review,
                             set_up_minimal_review,
                             render_minimal_place_details)
from review.models import Review, RatingCategory
from oprapp.helpers import (set_recently_viewed,
                            get_user_recently_viewed_elements,
                            check_if_place_exists)
from review.templatetags.review_tags import get_reviews
from oneplanet import settings
from .serializers import CreateReviewSerializer
from oprapp.models import Element
from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
from oprapp.templatetags.tags import (get_element,
                                      get_ratings_breakdown,
                                      get_element_review_category_mode,
                                      vote_exists,
                                      get_tailored_type_text)

def json_response(data):
    return HttpResponse(json.dumps(data), content_type='application/javascript; charset=utf8')


class Search(APIView):
    def get(self, request):

        search_results = get_search_results(request.GET["keyword"], request.GET["type"])

        filtered_result = []

        for category in search_results:
            for element in category:


                new_element = {
                    "image": "",
                    "name": element["name"],
                    "place_id": element["place_id"],
                    "most_common_rating": get_element_review_category_mode(get_element(element["place_id"]),
                                                                           "General"),
                    "ratings_count": get_reviews(get_element(element["place_id"])).count(),
                    "location": {"longitude": element["geometry"]["location"]["lng"],
                                 "latitude": element["geometry"]["location"]["lat"]
                                 }
                }

                if "photos" in element:
                    new_element["image"] = "https://maps.googleapis.com/maps/api/place/photo?maxheight=200&maxwidht=180&photoreference=" + str(
                        (element['photos'][0]['photo_reference'])) + "&key=AIzaSyDzCcuh5e2z9diD7o6jH0euGPVS-9GV6xk"

                filtered_result.append(new_element)

        return json_response(filtered_result)


class Details(APIView):
    def get(self, request):
        place_id = request.GET["place_id"]
        if "/" in place_id:
            return json_response({"details":place_id+" is not a valid place id"})

        google_details = requests.get(
            "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + place_id + "&key=" + settings.PLACES_API_KEY).json()


        details = {"place_id": place_id,
                   "image":"",
                   "name": google_details["result"]["name"],
                   "location": {"longitude": google_details["result"]["geometry"]["location"]["lng"],
                                "latitude": google_details["result"]["geometry"]["location"]["lat"]
                                }}
        # append the photo if there was any photos
        if "photos" in google_details["result"]:
            details[
                "image"] = "https://maps.googleapis.com/maps/api/place/photo?maxheight=200&maxwidht=180&photoreference=" + str(
                (google_details["result"]['photos'][0]['photo_reference'])) + "&key=AIzaSyDzCcuh5e2z9diD7o6jH0euGPVS-9GV6xk"



        element = get_element(place_id)
        categories = RatingCategory.objects.all().exclude(name="General")
        most_common_scores = {}
        non_general_categories = []

        # get most common ratings for all categories
        for cat in categories:

            if cat.parent is None:
                sub_categories = RatingCategory.objects.all().filter(parent=cat)

                non_general_categories.append({"name": cat.name,
                                               "description": cat.question,
                                               "rating": get_element_review_category_mode(element, str(cat)),
                                               "subCategories": [{"name": sub_cat.name,
                                                                  "description": sub_cat.question,
                                                                  "rating": get_element_review_category_mode(element,
                                                                                                             str(
                                                                                                                 sub_cat)),
                                                                  } for sub_cat in sub_categories],
                                               })
        general_cat = RatingCategory.objects.get(name="General")
        most_common_scores["mainCategory"] = {"name": general_cat.name,
                                              "description": general_cat.question,
                                              "rating": get_element_review_category_mode(element, str(general_cat)),
                                              }
        most_common_scores["categories"] = non_general_categories

        details["categories_tree"] = most_common_scores
        details["ratings_breakdown"] = get_ratings_breakdown(element)
        details["review_count"] = get_reviews(element).count()
        details["description"] = {}
        details["description"]["address"] = google_details["result"]["formatted_address"]
        return json_response(details)


class CreateReviewView(GenericAPIView):
    """
      checks for authentication, if the right token exists,
      creates a review
      """
    serializer_class = CreateReviewSerializer
    # make sure the user is authenticated
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        if request.data.get("place_id") is None:
            return Response({"place_id": ["this field is required"]})
        serializer = CreateReviewSerializer(request.user, request.data.get("place_id"), data=request.data)
        serializer.is_valid(raise_exception=True)
        review_instance = serializer.save()
        details = set_up_review(request, review_instance)

        return Response(details)


class ReviewDeleteView(DestroyAPIView):
        """
        Deletes a review,
        returns appropriate response
        """
        permission_classes = (IsAuthenticated,)

        def delete(self, request, *args, **kwargs):
            review_id = request.data.get("review_id")
            review = get_object_or_404(Review, pk=review_id)
            review.delete()
            return Response(
                {"detail":"Review was successfully deleted."},
                status=status.HTTP_200_OK
            )


# get reviews of a place
class Reviews(APIView):

    def get(self, request):

        place_id = request.GET["place_id"]

        element = get_element(place_id)

        element_reviews = get_reviews(element)

        reviews = []

        for review in element_reviews:
            reviews.append(set_up_minimal_review(request, review))

        return json_response(reviews)

class GetReview(APIView):

    def get(self, request):
        review_id = request.GET["review_id"]
        review = Review.objects.get(pk=review_id)
        get_review = set_up_review(request, review)
        return json_response(get_review)

class GetUserRecentlyViewedPlaces(APIView):

    permission_classes = (IsAuthenticated,)
    @csrf_exempt
    def get(self, request):
        if request.user.is_authenticated():
            viewed_places = get_user_recently_viewed_elements(request)
            rendered_places = [render_minimal_place_details(place[1]) for place in viewed_places]
            return Response({"places":rendered_places,
                             "viewed_count": len(viewed_places)
                                  },status=status.HTTP_200_OK)
        else:
            return Response({"detail ": "Authentication failed"},
                            status=status.HTTP_401_UNAUTHORIZED)

class SetUserRecentlyViewedPlaces(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        place_id = request.data.get("place_id")

        if request.user.is_authenticated():
            if place_id is None:
                return json_response({"detail ":"place_id is required"})
            # set the recently viewed place
            raw_element_details = set_recently_viewed(request.user, place_id)
            # get the place details from cache or google details
            element_details = check_if_place_exists(raw_element_details)

            if element_details is not None:
                details = render_minimal_place_details(element_details)
                return Response(details, status= status.HTTP_200_OK)
            else:
                return Response({"detail ":"we could not find the place you requested"}, status=status.HTTP_404_NOT_FOUND)
        else:
            return json_response({"detail ":"Authentication failed"})


class Vote(GenericAPIView):

    permission_classes = (IsAuthenticated,)

    def get(self):
        pass

    def post(self,request):
        review_id = request.data.get("review_id")
        print(review_id)
        vote_type = request.data.get("vote_type")
        data = dict()

        data["vote_type"] = vote_type
        review = Review.objects.get(pk=review_id)

        if vote_type == "like":

            # if user has already liked this review
            if review.votes.exists(request.user.pk, action=0):
                review.votes.delete(request.user.pk)
                data["like_removed"] = True

            # if user has already disliked this review
            elif review.votes.exists(request.user.pk, action=1):
                review.votes.delete(request.user.pk)
                data["dislike_removed"] = True
                review.votes.up(request.user.pk)
                data["like_added"] = True
            # the user has not liked or disliked the video
            else:
                review.votes.delete(request.user.pk)
                review.votes.up(request.user.pk)
                data["like_added"] = True

        if vote_type == "dislike":

            # if user has already liked this review
            if review.votes.exists(request.user.pk, action=0):
                review.votes.delete(request.user.pk)
                data["like_removed"] = True
                review.votes.down(request.user.pk)
                data["dislike_added"] = True

            # if user has already disliked this review
            elif review.votes.exists(request.user.pk, action=1):
                review.votes.delete(request.user.pk)
                data["dislike_removed"] = True
            # the user has not liked or disliked the video
            else:
                review.votes.delete(request.user.pk)
                review.votes.down(request.user.pk)
                data["dislike_added"] = True

        # article.refresh_from_db()
        data["likes"] = review.votes.count(action=0)
        data["dislikes"] = review.votes.count(action=1)
        data["review_id"] = review_id

        return json_response(data)


class Category(APIView):

    def get(self, request):

        details = {}
        categories = {}
        ratings_tree = []

        for rating_ in RatingCategory.objects.all():

            if rating_.name == "General":
                categories["mainCategory"] = {"name": rating_.name,
                                              "description": rating_.question,
                                              "rating": 0,
                                              }

            else:

                if rating_.parent is None:
                    ratings_tree.append(
                        {"name": rating_.name,
                         "description": rating_.question,
                         "rating": 0,
                         "subCategories": [{"name": sub_cat.name,
                                            "description": sub_cat.question,
                                            "rating": 0,
                                            } for sub_cat in RatingCategory.objects.all() if
                                           sub_cat.parent == rating_]
                         }
                    )

        categories["categories"] = ratings_tree
        details["categories_tree"] = categories

        return json_response(details)
