from django.conf.urls import url, include

from oprapp import views
from restapi.views import (Search,
                           Details,
                           Reviews,
                           CreateReviewView,
                           Vote,
                           GetReview,
                           ReviewDeleteView,
                           GetUserRecentlyViewedPlaces,
                           SetUserRecentlyViewedPlaces,
                           Category)

urlpatterns = [

    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/', include('rest_auth.rest_registration.urls')),
    url(r'^search/', Search.as_view()),
    url(r'^place/', Details.as_view()),
    url(r'^review/create/', CreateReviewView.as_view()),
    url(r'^review/delete/', ReviewDeleteView.as_view()),
    url(r'^get-review/', GetReview.as_view()),
    url(r'^reviews/', Reviews.as_view()),
    url(r'^vote/', Vote.as_view()),
    url(r'^categories/', Category.as_view()),
    url(r'^recently-viewed-places/create/', SetUserRecentlyViewedPlaces.as_view()),
    url(r'^recently-viewed-places/', GetUserRecentlyViewedPlaces.as_view()),
]
