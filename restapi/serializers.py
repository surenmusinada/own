from rest_framework import serializers
from django.utils.translation import get_language
from review.models import Review, Rating, RatingCategory
from oprapp.models import Element
from django.contrib.contenttypes.models import ContentType
import re


class CreateReviewSerializer(serializers.ModelSerializer):
    when = serializers.DateField()
    def __init__(self, user, place_id, **kwargs):
        super(CreateReviewSerializer, self).__init__(**kwargs)
        self.reviewed_item = Element.objects.get(place_id=place_id)
        self.user = user

        self.fields["title"].required= False
        self.fields["content"].required= False

        for category in RatingCategory.objects.all():
            field_name = 'category_{0}'.format(category.name)
            self.fields[field_name] = serializers.CharField(
                help_text=category.question,
            )
            # make general sustainability rating required
            if category.name == "General":
                self.fields[field_name].required = True
            else:
                self.fields[field_name].required = False

    def validate(self, attrs):
        for category in attrs:
            # accept a value between 1 and 5 inclusive of bounds
            if category.startswith('category_') and attrs.get(category) is not None:
                # check if the value contains any letters
                if re.search('[a-zA-Z]', attrs.get(category)) is not None or int(attrs.get(category)) not in range(1, 6):
                    raise serializers.ValidationError(
                        "Rating categories only accept a value between 1 and 5 inclusive")
        return attrs

    class Meta:
        model = Review
        fields = ('title', 'content', 'when', 'type',)

    def create(self, validated_data):
        return Review.objects.create(
            title=validated_data.get('title'),
            content=validated_data.get('content'),
            type=validated_data.get('type'),
            when=validated_data.get('when'),
            object_id=validated_data.get('object_id'),
            content_type=validated_data.get("content_type")
        )

    def save(self):

        self.instance = super(CreateReviewSerializer, self).save(
            content_type=ContentType.objects.get_for_model(Element),
            object_id=self.reviewed_item.pk)
        self.instance.user = self.user
        self.instance.language = get_language()

        # create rating objects for this review
        for category in self.fields:
            # accept a value between 1 and 5 inclusive of bounds
            if category.startswith('category_') and self.validated_data.get(category) is not None:
                rating, created = Rating.objects.get_or_create(
                    review=self.instance,
                    category=RatingCategory.objects.get(
                        name=category.replace('category_', '')),
                )
                rating.value = self.validated_data.get(category)
                rating.save()

        self.instance.save()
        return self.instance
