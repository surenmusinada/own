from django.conf.urls import url

from rest_auth.views import (
    LoginView, LogoutView, UserDetailsView, PasswordChangeView,
    PasswordResetView, PasswordResetConfirmView, UserDeleteView, GetUserReviews
)

urlpatterns = [
    # URLs that do not require a session or valid token
    url(r'^password/reset/$', PasswordResetView.as_view(),
        name='rest_password_reset'),
    url(r'^password/reset/confirm/$', PasswordResetConfirmView.as_view(),
        name='rest_password_reset_confirm'),
    url(r'^login/$', LoginView.as_view(), name='rest_login'),
    # URLs that require a user to be logged in with a valid session / token.
    url(r'^logout/$', LogoutView.as_view(), name='rest_logout'),
    # get the currently logged in user
    url(r'^get-user/$', UserDetailsView.as_view(), name='rest_user_details'),
    # delete the currently logged in user
    url(r'^user/delete/$', UserDeleteView.as_view(), name='rest_user_delete'),
    # get reviews of the currently logged in user
    url(r'^get-reviews/$', GetUserReviews.as_view(), name='rest_user_reviews'),
    # change password of the currently logged in user
    url(r'^password/change/$', PasswordChangeView.as_view(),
        name='rest_password_change'),
]
