
from django.dispatch import Signal

# user_registered_api signal
user_registered_api = Signal(providing_args=["user", "data"])