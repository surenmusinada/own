from oprapp.templatetags.tags import get_list_mode
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def get_rated_questions(element_ratings):

    if len(element_ratings)>0:

        rated_questions = []
        for rating in element_ratings:
            rated_questions.append({
                "questionText": rating.question.text,
                "value":rating.value })

        return rated_questions

def get_rated_questions_by_category(element_ratings, rating_category):
    rated_questions_ob = {}

    # if any reviews
    if len(element_ratings) > 0:

        for rating in element_ratings:

            if rating.question.rating_category == rating_category:

                try:
                    rated_questions_ob[rating.question.pk]["values"].append(int(rating.value))
                except KeyError:
                    rated_questions_ob[rating.question.pk] = {
                        "text": rating.question.text,
                        "values": []
                    }
                    rated_questions_ob[rating.question.pk]["values"].append(int(rating.value))

    rated_questions = []

    if len(rated_questions_ob) > 0:

        for rated_question in rated_questions_ob:

            rated_questions.append({
                "questionText": rated_questions_ob[rated_question]["text"],
                "value": get_list_mode(rated_questions_ob[rated_question]["values"])
            })

    return rated_questions


def paginate_objects(page_number, objects, paginate_by):
    objects_paginator = Paginator(objects, paginate_by)
    try:
        paginated_objects = objects_paginator.page(page_number)
    except PageNotAnInteger:
        paginated_objects = objects_paginator.page(1)
    except EmptyPage:
        paginated_objects = objects_paginator.page(objects_paginator.num_pages)

    return paginated_objects
