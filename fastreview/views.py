from django.contrib.contenttypes.models import ContentType
from django.http import JsonResponse
import json
from django.core import serializers
from django.shortcuts import render
import datetime
import pytz
from fastreview.helpers import (get_rated_questions_by_category,
                                get_rated_questions,
                                paginate_objects)
from restapi.helpers import set_up_review
from fastreview.models import ElementRating, RatingQuestion
from oprapp.models import Element, SearchCategory
from review.models import Review, Rating, RatingCategory
from fastreview.templatetags.fastreview_tags import get_cat_desc_by_name

# load rated questions for an entity
def rated_questions_entities(request):
    if request.is_ajax() and request.GET:
        element = Element.objects.get(place_id=request.GET.get('element_id'))
        rating_category = RatingCategory.objects.get(pk=request.GET.get('category_id'))

        # get all rated questions for this element
        element_ratings = ElementRating.objects.filter(entity=element)

        rated_questions = get_rated_questions_by_category(element_ratings, rating_category)

        return JsonResponse({"ratedQuestions": rated_questions})


def rated_questions_reviews(request):
    if request.is_ajax() and request.GET:
        element = Element.objects.get(place_id=request.GET.get('element_id'))
        review = Review.objects.get(pk=request.GET.get('review_id'))

        element_ratings = ElementRating.objects.filter(review=review, entity=element)

        review_questions = get_rated_questions(element_ratings)

        return JsonResponse({"reviewQuestions": review_questions})


def load_questions_by_category(request):

    if request.is_ajax() and request.GET:
        # get element
        element = Element.objects.get(place_id=request.GET.get("element_id"))
        # get the rating category
        rating_category=RatingCategory.objects.get(pk=request.GET.get("category_id"))
        # get search category
        search_category = SearchCategory.objects.get(name=element.category)
        # filter the questions based on rating category and element category
        categorized_questions=RatingQuestion.objects.filter(
            category=search_category,
            rating_category= rating_category)

        # paginate the retrieved questions by 5
        questions = paginate_objects(request.GET.get('page_number', 1),
                                     categorized_questions,
                                     3)
        # serialize paginated questions
        serialized_questions = serializers.serialize("json", questions)
        # return serialized questions
        return JsonResponse({"questions": serialized_questions})

def load_questions(request):
    if request.method == "GET":
        # load all of the questions
        if request.user.is_authenticated():
            element_id = request.GET.get("element_id", "")
            element = Element.objects.get(place_id=element_id)
            element_category = SearchCategory.objects.get(name=element.category)
            # filtering questions
            # retrieve all questions with the right search category
            categorized_questions = RatingQuestion.objects.filter(category=element_category)
            rated_elements = ElementRating.objects.filter(user=request.user, entity=element)

            # all ready rated questions for this place by the current logged in user
            already_rated_for_this_place = []

            for rated_element in rated_elements:
                print(rated_element.date_time)
                # if two hours has not passed since has last rating
                if abs(datetime.datetime.utcnow().replace(
                        tzinfo=pytz.UTC) - rated_element.date_time) < datetime.timedelta(hours=2):
                    already_rated_for_this_place.append(rated_element.question)

            rateable_questions = []
            # return those questions that are not in the rated questions
            for question in categorized_questions:
                if question not in already_rated_for_this_place:
                    rateable_questions.append(question)

            # paginate questions for ajax loading
            # return the first page at first
            page = request.GET.get('page_number', 1)

            # paginate questions by 2
            questions = paginate_objects(page, rateable_questions, 2)

            serialized_questions = serializers.serialize("json", questions)

            # get all rating categories except General
            rating_categories_objects = RatingCategory.objects.all().exclude(name="General")
            rating_categories= paginate_objects(request.GET.get('page_number', 1),
                                                          rating_categories_objects,2)

            # get personalized text
            rating_categories_list = []
            for rating_category in rating_categories:
                rating_categories_list.append({
                    "pk":rating_category.pk,
                    "name":rating_category.name,
                    "help_text":get_cat_desc_by_name(rating_category.name, element)

               })

            return JsonResponse({"questions": serialized_questions,
                                 "rating_categories": rating_categories_list})
    else:
        return JsonResponse({"signin": "signin"})


# rate general sustainability from sidebar
def fast_review(request):
    if request.is_ajax() and request.POST:
        # get required dat

        data = json.loads(request.POST.get('json_data'))
        element_id = data["element_id"]
        when = data["date"]
        type = data["type"]
        general_rating_value = data["rating_value"]
        rated_questions = data["rated_questions"]
        rating_categories = data["rated_categories"]

        # check if the required fields are not none or empty
        if element_id and when and type is not None or "":
            # get the reviewed_item
            reviewed_item = Element.objects.get(place_id=element_id)
            # create the review
            review, created = Review.objects.get_or_create(user=request.user,
                                                           content_type=ContentType.objects.get_for_model(Element),
                                                           object_id=reviewed_item.pk,
                                                           title="",
                                                           content="",
                                                           when=when,
                                                           type=type
                                                           )
            # save the review
            review.save()
            # create the general sustainability rating for this review
            rating, created = Rating.objects.get_or_create(
                review=review,
                category=RatingCategory.objects.get(
                    name="General")
            )
            rating.value = general_rating_value
            rating.save()

            # create ratings other than general rating for this review
            if len(rating_categories) > 0:
                for rating_category in rating_categories:
                    rating, created = Rating.objects.get_or_create(
                        review=review,
                        category=RatingCategory.objects.get(
                            pk=rating_category["id"])
                    )
                    rating.value = rating_category["value"]
                    rating.save()

            # get review freshly from db
            new_review = Review.objects.get(pk=review.pk)

            # create element ratings if any rating questions
            if len(rated_questions) > 0:
                for rating_question in rated_questions:
                    rated_element_question, created = ElementRating.objects.get_or_create(
                        user=request.user,
                        question=RatingQuestion.objects.get(pk=rating_question["id"]),
                        review=new_review,
                        entity=Element.objects.get(place_id=element_id),
                        value=rating_question["value"]
                    )

                    rated_element_question.save()

            details = set_up_review(request, new_review)
            return JsonResponse(details)

        return JsonResponse({"detail": "Validation error"})
