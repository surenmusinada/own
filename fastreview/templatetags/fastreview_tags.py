
from django.conf import settings
from review.models import RatingCategory, RatingCategoryDescriptions
from oprapp.models import SearchCategory
from django import template
from django.core.exceptions import ObjectDoesNotExist
register = template.Library()

@register.simple_tag
def get_cat_desc_by_name(category_name, element):

    if category_name is  " " or None:
        return False

    category = RatingCategory.objects.get(name=category_name)

    try:
        element_category = SearchCategory.objects.get(name=element.category)
        description_obj = RatingCategoryDescriptions.objects.get(rating_category=category,
                                                                 element_category=element_category)
    except ObjectDoesNotExist:
        # return the default text
        return category.question
        # return the personalized text
    return description_obj.help_text
