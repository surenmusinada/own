from __future__ import unicode_literals

from django.db import models
from oneplanet import settings

# Create your models here.
class RatingQuestion(models.Model):

    category = models.ForeignKey('oprapp.SearchCategory',  related_name='category', on_delete=models.CASCADE)
    rating_category = models.ForeignKey('review.RatingCategory', related_name='rating_category', on_delete=models.CASCADE)
    text = models.TextField()

    class Meta:
        verbose_name_plural = 'Rating Questions'

    def __str__(self):
        return '%s... %s -%s ' % (self.text[0:100], self.category, self.rating_category)



class ElementRating(models.Model):
    # the user who rated the question
    user = models.ForeignKey(
        getattr(settings, 'AUTH_USER_MODEL', 'auth.User'),
        verbose_name=('User'),
        related_name='user',
        blank=True, null=True,
    )
    # the question rated by the user
    question = models.ForeignKey('RatingQuestion', related_name='element_questions', on_delete=models.CASCADE)
    # the related review
    review = models.ForeignKey('review.Review', related_name='element_ratings', on_delete=models.CASCADE)
    # the element that was rated
    entity = models.ForeignKey('oprapp.Element', related_name='rated_element', on_delete=models.CASCADE)
    # the time of the rating
    date_time = models.DateTimeField(auto_now_add=True, null=False)
    # the value (0-5)
    value = models.CharField(max_length=20, default="0", null=True)

    def get_value(self):
        if self.value != "":
            num_value = int(float(self.value))
            return num_value
        return None

    class Meta:
        verbose_name_plural = 'Element Ratings'

    def __str__(self):
        return "{:.150}... _{}_{}".format(self.entity, self.user, self.question)
