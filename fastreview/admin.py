from django.contrib import admin
from .models import ElementRating
from .models import RatingQuestion

# Register your models here.
admin.site.register(ElementRating)
admin.site.register(RatingQuestion)