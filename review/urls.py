"""URLs for the review app."""
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^(?P<pk>\d+)/delete/$',
        views.ReviewDeleteView.as_view(),
        name='review_delete'),
    url(r'^(?P<pk>\d+)/update/$',
        views.ReviewUpdateView.as_view(),
        name='review_update'),
    url(r'^(?P<pk>\d+)/$',
        views.ReviewDetailView.as_view(),
        name='review_detail'),
    url(r'^(?P<content_type>[-\w]+)/(?P<object_id>\d+)/create',
        views.ReviewCreateView.as_view(),
        name='review_create'),
    url(r'^(?P<content_type>[-\w]+)/(?P<object_id>\d+)/create/(?P<search_type>[-\w]+)',
        views.ReviewCreateView.as_view(),
        name='review_create'),
    # get help text
    url(r'help-text/$',
        views.get_category_help_text,
        name='get_category_help_text'),

]
