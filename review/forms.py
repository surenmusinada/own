"""Forms for the ``review`` app."""
from django import forms
from django.conf import settings
from django.utils.translation import get_language
from django_libs.loaders import load_member
import json
from fastreview.models import ElementRating, RatingQuestion
from oprapp.models import Element
from .models import Review, Rating, RatingCategory, ReviewedElements


class ReviewForm(forms.ModelForm):
    general_cat_name = ""

    def __init__(self, reviewed_item, user=None, *args, **kwargs):
        self.user = user
        self.reviewed_item = reviewed_item
        self.widget = load_member(
            getattr(settings, 'REVIEW_FORM_CHOICE_WIDGET',
                    'django.forms.widgets.Select')
        )()

        super(ReviewForm, self).__init__(*args, **kwargs)

        # Dynamically add fields for each rating category
        for category in RatingCategory.objects.all():

            field_name = 'category_{0}'.format(category.pk)

            choices = category.get_choices()

            self.fields[field_name] = forms.ChoiceField(
                choices=choices, label=category.name,
                help_text=category.question,
                widget=self.widget,
            )

            # make general sustainability rating required
            if category.name == "General":
                self.fields[field_name].required = True
                self.general_cat_name = field_name
            else:
                self.fields[field_name].required = False

            if self.instance.pk:
                try:
                    self.initial.update({
                        'category_{0}'.format(category.pk): Rating.objects.get(
                            review=self.instance, category=category).value,
                    })
                except Rating.DoesNotExist:
                    pass

                    # review form attributes

        type_choices = (
            ('Business', 'Business'),
            ('Family', 'Family'),
            ('Couple', 'Couple'),
            ('Friends', 'Friends'),
            ('Solo', 'Solo'),
        )

        # we are gonna use this field to send rated questions in json_form
        self.fields["json_data_helper"] = forms.CharField(max_length=2024, required=False)

        self.fields["type"] = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=type_choices)
        self.fields["content"] = forms.CharField(widget=forms.Textarea(attrs={
            "cols": "8",
            "rows": "8",
            "placeholder": "Write anything you want about your experience."
                           " This could be sustainability related or it could be general thoughts."
                           " Tell us about things you really appreciated or disliked or give recommendations for others."
                           " For example:"
                           " Did the hotel have clear signs and tips for how to conserve energy?"
                           " Did the restaurant offer vegetarian or organic menu items?"
                           " Did the architecture blend in well with the natural surroundings?",
            "class": "form-control"
        }

        ), label="Review", required=False)
        # make when field required
        self.fields["when"] = forms.DateField(widget=forms.DateInput(format='%Y-%m-%d'),
                                              input_formats=('%Y-%m-%d',))

        self.fields["title"] = forms.CharField(widget=forms.TextInput(attrs={
            "class": "form-control",
            "placeholder": "Summarize your experience or highlight something special, bad or good.",
        }), label="Title of review", required=False)

    class Meta:
        model = Review
        fields = ("title", "content", "when", "type",)

    def save(self, *args, **kwargs):
        if not self.instance.pk:
            self.instance.user = self.user
            self.instance.reviewed_item = self.reviewed_item
            self.instance.language = get_language()
        self.instance = super(ReviewForm, self).save(*args, **kwargs)


        # get rated questions from json data
        json_data = json.loads(self.cleaned_data['json_data_helper'])
        rated_questions = json_data["rated_questions"]

        # create element ratings if any rating questions
        if len(rated_questions) > 0:
            for rating_question in rated_questions:
                rated_element_question, created = ElementRating.objects.get_or_create(
                    user=self.user,
                    question=RatingQuestion.objects.get(pk=rating_question["id"]),
                    review=self.instance,
                    entity=Element.objects.get(place_id=self.reviewed_item),
                    value=rating_question["value"]
                )
                rated_element_question.save()



        # Update or create ratings
        for field in self.fields:

            if field.startswith('category_') and self.cleaned_data[field] != "":
                rating, created = Rating.objects.get_or_create(

                    review=self.instance,
                    category=RatingCategory.objects.get(
                        pk=field.replace('category_', '')),
                )

                rating.value = self.cleaned_data[field]
                rating.save()

        self.instance.average_rating = self.instance.get_average_rating()

        self.instance.save()
        return self.instance

    def clean(self):
        cleaned_data = super(ReviewForm, self).clean()
        general_cat = cleaned_data.get(self.general_cat_name)

        if general_cat is None:
            raise forms.ValidationError(
                "General Sustainability Rating is required"
            )

        return cleaned_data

