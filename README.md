# README #

### What is this repository for? ###
 
 All of the one planet rating code will be uploaded to this repo by everyone, the repository will function as the official development repo of the project. every one of us can create a branch and work on specific tasks and we will merge them together after the work has been tested and approved by the team.
 

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies

Django==1.10.6
django-bootstrap-form==3.2.1
django-bootstrap3==8.2.1
django-ckeditor==5.2.2
django-generic-positions==0.2.2
django-libs==1.67.12
django-mptt==0.8.7
django-registration==2.2
django-user-media==1.2.4
django-vote==2.1.4
django-widget-tweaks==1.4.1
djangorestframework==3.5.1
googlemaps==2.4.6
Pillow==4.1.1
psycopg2==2.7.1
py2app==0.7.3
pyobjc-core==2.5.1
requests==2.11.1
simplejson==3.10.0
statistics==1.0.3.5
whitenoise==3.3.0
python-memcached


* Caching configuration

1- first install memcached: brew install memcached or sudo apt-get install memcached
2- install python-memcached: pip install python-memcached
3- check if memcached is installed and working by: memcached -vv, and you’ll see some server-like output.
4- run memcached as a daemon by: brew services start memcached or sudo service memcached start

how to run this project on your local computer:

1- make sure you have python installed on your computer
2- you can use python's package manager "pip" to install Django
3- once Django is installed, open your command line or terminal if you are on a mac os and navigate to the project's main folder, then run the following commands. ( python manage.py migrate ) to migrate the DB. after you have successfully migrated the database, then run ( python manage.py runserver ) to run the project on the local server. then you can open the project in your web browser through the IP provided in the command line.
if you wish to access the admin section of the website, you should first create an admin, and you can do so by running ( python manage.py createsuperuser) and providing your details when prompted. after you have created a superuser you can open your browser and type ipaddress/admin and log in.
note: the parenthesis are not part of the commands. and if you have any problem while installing any of the requirements or Django, do not forget to run the commands as administrator ( use sudo before the commands).


### Contribution guidelines ###

1- make sure you create your personal branch 
2- do not commit to master/merge before the code has been reviewed, tested and approved by the team.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact