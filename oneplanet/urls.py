"""oneplanet URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url, include
from django.contrib import admin
from registration.backends.hmac.views import RegistrationView
# from registration.backends.model_activation.views import RegistrationView
from oprapp import views
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from oprapp.forms import SignUpForm
from oprapp.forms import UpdateProfileForm
from cuser.forms import AuthenticationForm
from django.conf import settings
from django.conf.urls.static import static
from helpcenter.views import terms_of_service, privacy_policy
from django.views.generic import ListView
from review.models import Review
from fastreview.views import (fast_review,
                              load_questions,
                              load_questions_by_category,
                              rated_questions_entities,
                              rated_questions_reviews)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    # rest api
    url(r'^api/', include('restapi.urls')),
    url(r'^accounts/register/$', RegistrationView.as_view(form_class=SignUpForm), name='registration_register'),
    url(r'^accounts/', include('registration.backends.hmac.urls')),
    url(r'^accounts/login/$', auth_views.login, {'authentication_form': AuthenticationForm}, name='login'),
    url(r'^accounts/logout/$', auth_views.logout, name='logout'),
    # user dashboard
    url(r'^dashboard/$', login_required(views.Dashboard.as_view()), name="dashboard"),
    # password reset
    url(r'^accounts/password_reset/$', auth_views.password_reset, name='password_reset'),
    # password reset done
    url(r'^password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    # password reset form
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm, name='password_reset_confirm'),
    # password reset done confirmation
    url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),
    # dashboard change password
    url(r'^dashboard/change-password/$', login_required(views.ChangePassword.as_view()), name='change_password'),
    # dashboard delete account
    url(r'^dashboard/delete-account/$', login_required(views.DeleteAccount.as_view()), name='delete_account'),
    # dashboard update profile information#
    url(r'^dashboard/(?P<pk>\d+)/update-profile/$', views.update_profile, name='update_profile'),
    # dashboard change password
    url(r'^dashboard/(?P<pk>\d+)/change-name/$', views.update_name, name='change_name'),
    # dashboard upload profile picture#
    url(r'^dashboard/upload-profile-pic/$', login_required(views.UploadProfilePicture.as_view()),
        name='upload_profile_picture'),
    # dashboard delete profile picture
    url(r'^dashboard/delete-profile-picture/$', views.delete_profile_picture, name='delete_profile_picture'),
    # dashboard change email#
    url(r'^dashboard/personal-email/$', views.change_email, name='change_email'),
    # give feedback #
    url(r'^give-feedback/$', views.feedback, name='give_feedback'),
    url(r'^feedback-confirm/$', views.feedback_confirm, name='feedback_confirm'),
    # help center
    url(r'^help/', include('helpcenter.urls')),
    # ckeditor
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    # legal
    url(r'^legal/terms', terms_of_service, name="terms_of_service"),
    url(r'^legal/privacy', privacy_policy, name="privacy_policy"),
    # index page
    url(r'^$', views.index, name="index"),
    # reviews part
    url(r'^review/', include('review.urls')),
    url(r'^element/(?P<place_id>[-\w]+)/(?P<search_type>[-\w]+)', views.ElementView.as_view(), name="element_view"),
    # element reviews
    url(r'^element/(?P<place_id>[-\w]+)/(?P<search_type>[-\w]+)/(?P<reviews>\d+)', views.ElementView.as_view(), name="element_view"),
    url(r'^element/(?P<place_id>[-\w]+)/(?P<reviews>\d+)', views.ElementView.as_view(), name="element_view"),
    url(r'^element/(?P<place_id>[-\w]+)/', views.ElementView.as_view(), name="element_view"),
    url(r'^review-listing/(.*)', views.reviewsOfElement, name='reviews_of_element'),
    url(r'^review-listing/', ListView.as_view(model=Review), name='review_list'),
    # review like and dislike
    url(r'^(?P<review_id>\d+)/vote/$', views.up_down_vote, name='like_dislike'),
    # add to favorites
    url(r'^add-to-favorites', views.add_to_favorites, name='add_to_favorites'),
    #remove from favorites
    url(r'^remove_from_favorites', views.remove_from_favorites, name='remove_from_favorites'),
    #invite a friend to OPR
    url(r'^invite', login_required(views.InviteFriend.as_view()), name='invite_friend'),
    # make a fast review from the sidebar
    url(r'^fast-review', fast_review, name='fast_review'),
    # load questions dynamically from sidebar
    url(r'^load-question', load_questions, name='load_question'),
    # load rated questions for every category for an Entity/Element
    url(r'^load-rated-question', rated_questions_entities, name='load_rated_question'),
    # load rated questions for every review
    url(r'^rated-questions-reviews', rated_questions_reviews, name='rated_questions_reviews'),
    # load questions based on category
    url(r'^category-load-questions', load_questions_by_category, name='load_question_category'),
]

# development environment
if settings.DEBUG:
    # upload image
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
