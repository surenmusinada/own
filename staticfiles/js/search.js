 /* display a map popup modal for every element */
    (function () {
    $("#view-on-map").on("click", function initialize() {
    if ($(this).hasClass("list-elements")){
    $(this).find("span").html("View on map");
    $("#search-result").fadeIn(1000);
    $("#map_wrapper").fadeOut(1000);
    $(this).removeClass("list-elements");
    $("#map-infobox").css({display: "none"});
    }else{
    $("#search-result").fadeOut(1000);
    $("#map_wrapper").fadeIn(1000);
    $(this).find("span").html("See the list");
    $(this).addClass("list-elements");
    // Multiple Markers
     var markers =[]
                 "{% for category in result %}"
                 "{% for place in category %}"
                 // get reviews number
                 {% get_element place.place_id as element %}
                 {% get_reviews_number element as review_number%}
                 {% get_element_review_category_mode element "General" as cat_mode %}
                     markers.push({
                      name: "{{place.name}}",
                      lat: "{{place.geometry.location.lat}}",
                      lng: "{{place.geometry.location.lng}}",
                      place_id: "{{place.place_id}}",
                      photo_ref:"{{place.photos.0.photo_reference}}",
                      review_number: "{{review_number}}",
                      mode: "{{cat_mode}}",
                      })
                 {% endfor %}
                 {% endfor %}
    //var bounds = new google.maps.LatLngBounds();
    // Display a map on the page
    var map = new google.maps.Map(document.getElementById("map_canvas"),{
        center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
        mapTypeId: 'roadmap',
        tilt: 45,
        fullscreenControl: false
    });
    //Create LatLngBounds object.
        var latlngbounds = new google.maps.LatLngBounds();
    // Info Window Content
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    // Loop through our array of markers & place each one on the map
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(parseFloat(markers[i]["lat"]), parseFloat(markers[i]["lng"]));
        var marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i]["name"]
        });
        //extend the bounds to include each marker's position
        latlngbounds.extend(marker.position);
        // Allow each marker to have an info window
        google.maps.event.addListener(marker, 'mouseover', (function(marker, i){
            return function(){
                 p_id= markers[i].place_id
                 p_name=markers[i].name
                infoWindow.setContent("<div style='width:200px;min-height:20px; '>"+p_name+"</div>");
                infoWindow.open(map, marker);
            }
        })(marker, i));
        // close window on mouse out
        google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
            return function() {
                infoWindow.close(map, marker);
            }
        })(marker, i));
        // Allow each marker to have an info window
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
            $("#map-infobox").css({display: "block"});
            photo_ref = markers[i].photo_ref
            $(".map-infobox-imgbox").find("img").attr("src","https://maps.googleapis.com/maps/api/place/photo?maxheight=100&maxwidht=100&photoreference="+photo_ref+"&key=AIzaSyDzCcuh5e2z9diD7o6jH0euGPVS-9GV6xk")
                  {% for category in result %}
                  {% for place in category %}
                 if(markers[i].place_id == "{{place.place_id}}"){
                 // delete the previous rating globes
                 var ratings = $(".map-infobox-contentbox ul li");
                 if(ratings.length >0){
                 ratings.each(function( index ) {
                 $(this).remove();
                 });
                 }
                 // end

                    {% get_element place.place_id as element %}
                    {% get_reviews_number element as review_number%}
                    {% get_element_review_category_mode element "General" as cat_mode %}
                    {% review_format  cat_mode as formatted_review %}
                    {% for x in formatted_review.full_globes %}
                    $(".map-infobox-contentbox").find("ul").append("<li><i class='rating-on'></i><li>");
                     {% endfor %}
                     {% for x in formatted_review.empty_globes %}
                    $(".map-infobox-contentbox ul").find("ul").append("<li><i class='rating-off'></i><li>");
                     {% endfor %}
                 }
                 {% endfor %}
                 {% endfor %}

            $(".map-infobox-contentbox").find("h4").html("<a href='/element/"+markers[i].place_id+"'>"+markers[i].name+"</a>");
            $("#view_rate_map").attr("href","/element/"+markers[i].place_id);
            /*scroll to the element start
            var offset = $("#map-infobox").offset();
            offset.left -= 20;
            offset.top -= 20;
            $('html, body').animate({
            scrollTop: offset.top,
            scrollLeft: offset.left
            });
            scroll to the element end */
            $("#map-infobox")[0].scrollIntoView({
            behavior: "smooth", // or "auto" or "instant"
            block: "start" // or "end"
            });
            var myLatlng = {lat: parseFloat(markers[i].lat), lng: parseFloat(markers[i].lng)};
            map.setCenter(myLatlng);
            }
        })(marker, i));
        //Get the boundaries of the Map.
        var bounds = new google.maps.LatLngBounds();
    }
     //Set the center of the map.
     map.setCenter(latlngbounds.getCenter());
     //now fit the map to the newly inclusive bounds
     map.fitBounds(latlngbounds);
     }// end else
     // might be moved to main.js
     $(".map-infobox .map-infobox-close").on("click", function(){
        $("#map-infobox").css({display: "none"});
     })
});
    }());
/* end display a map popup modal */