

// autofill review form
function sendDataViaUrl(){
   var currentLocation = $("#add-review").attr("href").trim();
   var generalRating = $('.general-rating .rating-input:checked').val();
   var vistType =  $('#typeSelect input[name=radio]:checked').val();
   var visitDate = $('#datepicker input[name=when]').val();

   // we are gonna add the general always for the sake of the question mark
   currentLocation = currentLocation+"?general_rating="+generalRating;

  if (visitDate){
    currentLocation = currentLocation+"&vist_date="+visitDate;
   }

  if ( vistType!= undefined){
    currentLocation = currentLocation+"&visit_type="+vistType;
   }

  // get all rated categories except general
  var ratedCategories = $('.rating-category-item .rating .rating-input:checked');

  // add rated categories to the url if any
  if (ratedCategories.length>0){
      ratedCategories.each(function(index,element){
      var categoryId = $(this).parent().attr("id");
      var categoryValue = $(this).val();
      currentLocation= currentLocation+"&"+categoryId+"="+categoryValue;
  });
  }
  // go to the place after creating the url
  window.location = currentLocation;

}

// start setup review form
function setUpReviewForm(){
// setup data of the review
$("#id_when").attr("value",findGetParameter("vist_date"));
// setup type
visitTypes= $("#id_type").find("input");
visitTypes.each(function(){
  if ($(this).val().toLowerCase()==findGetParameter("visit_type")){
    $(this).prop("checked", true);
  }
});

/* start setting up rating categories */
// setup general category
var generalValue= findGetParameter("general_rating");
if (generalValue!=null){
  // get rating inputs
  var generalInputs = $("#general_rating").find("input");
  // set up value
  setUpRatingCategoryValue(generalValue,generalInputs);
}

// get all rating categories other than general
 var ratedCategories = $('.rating');

// loop over all of them
 ratedCategories.each(function(){
    // get its id
  var ratingCategoryId = $(this).attr("id");
    // get its value
    var ratingCategoryValue = findGetParameter(ratingCategoryId);
    // if it has a value other than none
   if(ratingCategoryValue!= null){
    // get all its inputs
    var ratingInputs = $(this).find(".rating-input");
    setUpRatingCategoryValue(ratingCategoryValue, ratingInputs);
   }
 });

} // end setup review form


function setUpRatingCategoryValue(value,ratingInputs){
ratingInputs.each(function(){
  if($(this).val()==value){
    $(this).prop("checked", true);
    // show the clear button
    $(this).parent().next(".clear-button").css({"display":"block"});
  }
});
}

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName){
            result = decodeURIComponent(tmp[1]);
          } 
        });
    if (result != undefined){
    	    return result;
    	}else{
        return null;
      }
}





