(function (){ /* start iife */

/* bootstrap datepicker */

$('#id_date_of_birth').datepicker({
 format: 'yyyy-mm-dd'
});

$('#id_when').datepicker({
 format: 'yyyy-mm-dd'
});

$("#id_when").addClass("form-control");


/* get current date */
var today = new Date();
var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();

/* when review field
$("#id_when").addClass("form-control").val(date).datepicker({
 format: 'yyyy-mm-dd'
})
*/


 /* drop-down */
 $(".dropdown-toggle").dropdown();

 /* accordion */

 /* accordion start help center */
     $(".help-accordion").on("click",function(event){
         /* prevent form submission */
         event.preventDefault();
         /* toggle class */
         $(this).toggleClass("active");
          var panel = this.nextElementSibling;
         var arrow = $(this).children().next("i");
         console.log(arrow);
         if (panel.style.maxHeight){
             panel.style.maxHeight = null;
             arrow.toggleClass("glyphicon-chevron-down").removeClass("glyphicon-chevron-up");
         } else {
             panel.style.maxHeight = panel.scrollHeight + "px";
             arrow.toggleClass("glyphicon-chevron-up").removeClass("glyphicon-chevron-down");
         }
     });

 /* end accordion help center end */

/* handles form loading */
 var loadForm = function(){
 var btn = $(this)

  $.ajax({
       url: btn.attr("data-url"),
       type: 'get',
       dataType: 'json',
       beforeSend: function () {
         $("#modal-dashboard").modal("show");
       },
       success: function (data) {
         $("#modal-dashboard .modal-content").html(data.html_form);
       }
     });
 }

 /* handles form submission */
 var saveForm = function () {
  var form = $(this);
  $.ajax({
       url: form.attr("action"), // get url from form
       data: form.serialize(),   // package data
       type: form.attr("method"), // get method type from form
       dataType: 'json',
       success: function (data) {
         if (data.form_is_valid) {
             $("#modal-dashboard").modal("hide");  // <-- Close the modal
           $("#notification").append("<div class='alert alert-success alert-dismissable'><p><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"
           +data.updated+"</p></div>");

           if(data.isName){
             $("#name_label").html(data.updated_user[0].first_name+" "+data.updated_user[0].last_name);
           }
           if(data.isProfile){
               // parse data
               profileData = JSON.parse(data.updated_profile);
               // get the profile object
               profile = profileData[0].fields;
               // in case profile about me was blank
               if(profile.about_me==""){
               profile.about_me = "Your about me section is currently blank";
               }
               $("#p_about_me_blank").html(profile.about_me);
               $("#p_about_me").html(profile.about_me);

               $("#p_city").html(profile.city);
               $("#p_gender").html(profile.gender);
               $("#p_birth_date").html(profile.birth_date);
           }
                /* upload , delete and change avatar */
           if(data.removeAvatar){
                    location.reload();
           }
         } else {
            // reload the modal
           $("#modal-dashboard .modal-content").html(data.html_form);
         }
       }
     });
     return false;
 }

/* change password */
$(".js-change-password").click(loadForm);
$("#modal-dashboard").on("submit", ".js-change-password-form", saveForm);
/* delete account */
$(".js-delete-account").click(loadForm);

/* change name */
$(".js-change-name").click(loadForm);
$("#modal-dashboard").on("submit", ".js-change-name-form", saveForm);
/* update profile */
$(".js-update-profile").click(loadForm);
$("#modal-dashboard").on("submit", ".js-update-profile-form", saveForm);
/* change email address */
$(".js-change-email").click(loadForm);
$("#modal-dashboard").on("submit", ".js-change-email-form",saveForm);
/* delete profile picture */
$(".js-delete-p-pic").click(loadForm);
$("#modal-dashboard").on("submit", ".js-delete-profile-pic-form",saveForm);

/* update profile picture */
$(".js-upload-profile-pic").click(loadForm);
/* for uploading picture */
$("#modal-dashboard").on("submit", ".js-upload-profile-pic-form", function (event) {
    var formData = new FormData($(this)[0]);
    $.ajax({
      url: form.attr("action"),
      data: formData,
      type: form.attr("method"),
      dataType: 'json',
      cache: false,
      contentType: false,
      processData: false,
      success: function (data){
        if (data.form_is_valid && data.uploadAvatar){
          $("#modal-dashboard").modal("hide");
          alert("Profile picture was uploaded");
          console.log("Uploaded");
        } else {
           // reload the modal
         $("#modal-dashboard .modal-content").html(data.html_form);
       }
      }
    });
    return false;
  });


  /* img preview */
  function readURL(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();
              reader.onload = function (e) {
                  $('#image_upload_preview').attr('src', e.target.result);
              }

              reader.readAsDataURL(input.files[0]);
          }
      }
  $("#modal-dashboard").on("change","#id_profile_picture", function(){
      readURL(this);
     });

  /* img preview end */




/* dashboard modal popup end */


/* category accordion start */

$('[data-toggle="tooltip"]').tooltip({trigger:"hover"});

 $(document).on('click', '.cat-accordion', function(event){
       /* prevent form submission */
             event.preventDefault();
             /* toggle class */
             $(this).toggleClass("active");
             var panel = $(this);
             panel = panel.next().next("div")[0];

             var arrow = $(this).find("i");
             if (panel.style.maxHeight){
                 panel.style.maxHeight = null;
                 arrow.toggleClass("glyphicon-menu-down").removeClass("glyphicon-menu-up");
             } else {
                 panel.style.maxHeight = panel.scrollHeight + "px";
                 arrow.toggleClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
             }
     });

/* category accordion end */


/* like-dislike voting start */

     // like
     $(document).on('click', '.js-up-vote', function(){
      obj = $(this);
      like_dislike(obj);
         });
        // dislike
         $(document).on('click', '.js-down-vote', function(){
              obj = $(this);
              like_dislike(obj);
         });


         var like_dislike = function(obj){
              vote_type = obj.attr("name");
              var csrftoken = getCookie('csrftoken');
                 $.ajax({
                 url: obj.attr("data-url"),
                 data: {vote_type : vote_type},
                 type: "post",
                 dataType: 'json',

                 beforeSend: function(xhr, settings){
                  if (!this.crossDomain){
                   xhr.setRequestHeader("X-CSRFToken", csrftoken);
                          }},
                 success: function (data) {
                    $("#num-likes-"+data["article_id"]).html(data["num_of_likes"])
                    $("#num-dislikes-"+data["article_id"]).html(data["num_of_dislikes"])
                    var prevObj = null;

                    if(data["up_vote_added"]){
                         obj.parent().next("p").find(".js-down-vote").removeClass("active_vote");
                         obj.addClass("active_vote");
                    }
                    if(data["down_vote_added"]){
                         obj.parent().prev("p").find(".js-up-vote").removeClass("active_vote");
                         obj.addClass("active_vote");
                    }
                    if(data["vote_removed"]){
                       obj.removeClass("active_vote");
                    }
                    if(data["vote_removed"] && data["down_vote_added"] ){
                    obj.parent().prev("p").find(".js-up-vote").removeClass("active_vote");
                         obj.addClass("active_vote");
                    }
                 },

                 fail: function (data){
                  console.log("something went wrong");
                 }

                 }); // ajax end
            };
    /* like-dislike voting end */

// using jQuery, getting a csrftoken
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

/* element rating category break-down start */
        $('.see-more').click(function(e){
            $("#element-specific-ratings").slideToggle("slow");
            console.log($(this).text())
            if ($(this).text().trim() == "See more".trim()){
              $(this).text("See less");
            }else{
              $(this).text("See more");
            }
        });

/* element rating category break-down end */

/* profile-picture */

img = $('#profile-pic');
    width = img.outerWidth();
    height = img.outerHeight();
    img.css("width", "40px");
    img.css("height", "40px");

/* end profile-pic */

/* tab content fix */
$("#go-to-reviews").on("click", function(e){
elementId= $(this).attr("href");
element = $(elementId);
element.closest('li').addClass("active").siblings().removeClass("active");
$(element.attr('href')).show().siblings('.tab-pane').hide();
});
$("#element-page .tab-pane").hide().first().show();
$("#element-page .nav-tabs li:first").addClass("active");
$("#element-page .nav-tabs a").on('click', function (e) {
 e.preventDefault();
$(this).closest('li').addClass("active").siblings().removeClass("active");
$($(this).attr('href')).show().siblings('.tab-pane').hide();
});


/* general rating info box start*/

$('#general-info').mouseover(function(){
    infoBox = $('.general-info-box').first();
    infoBox.fadeIn(500);
}).mouseout(function(){
    infoBox.fadeOut(500);
});

/* general rating info box end*/


/* clear ratings section */

/* clear ratings section */
$(document).on("click",".clear-button", function(event){
/* prevent form submission */
   event.preventDefault();
   resetButton = $(this)
   var nextDiv = resetButton.parent().find(".fieldWrapper");
   var ratingStars = nextDiv.children("input");
      ratingStars.each(function() {
        $(this).prop('checked', false);
        resetButton.css({"display":"none"});
      });
});

/* show the clear rating button */
$(document).on("change",".rating-input", function(){
var resetButton = $(this).closest("div").nextAll("button");
    resetButton.css({"display":"block"});
});

/* clear ratings section end */

/* set search button value */
$(".cc-selector input").on("click", function(){
$("#search_btn").html("Search "+ $(this).attr("value"))
selected_category_value = ($(this).attr("value"))
});

$(".search-categories-list").children().on("click", function(){

var current_li_cat = $(this).find("a").html();
/* set search button value */
$("#search_btn").html("Search "+current_li_cat)
var search_types = document.querySelectorAll(".cc-selector input")
/* uncheck all */
 var searchCategoriesSiblings = $(".cc-selector input");
      searchCategoriesSiblings.each(function(){
        $(this).checked = false;
      });

var foundMatchWithIcon = false
for (var j =0; j < search_types.length; j++){
current_cat_value = search_types[j].getAttribute("value").trim();
/* check the one the right one */
if (current_li_cat.trim() == current_cat_value){
search_types[j].checked = true;
foundMatchWithIcon = true
break;
}}

// in case user selects a category that is not shown as an icon
if(!foundMatchWithIcon){
searchCategory = document.getElementById("helper_search_type");
searchCategory.setAttribute("value", current_li_cat)
searchCategory.checked = true;
}
});
// validate search_form
$(".search_form").on("submit",function(event){
  if ($("#id_Place").val()== ""){
      $("#id_Place").addClass("red-placeholder");
      event.preventDefault();
    }
});
// end

/* categories search bar end */

/* infinite scrolling */

/* infinite scrolling end */
 var infinite = new Waypoint.Infinite({
      element: $('.infinite-container')[0],
      onBeforePageLoad: function () {
        $('.loading').show();
        $('[data-toggle="tooltip"]').tooltip({trigger:"hover"});
      },
      onAfterPageLoad: function ($items) {
        $('.loading').hide();
        $('[data-toggle="tooltip"]').tooltip({trigger:"hover"});
      }
    });

}()) /* end iife */
