// start iife
(function(){


var hideOnClick = false;
var isLargeWindow = false;


/*
 *
 * mobile version sidebar element.html
 *
 */

$(window).scroll(function() {
    if ($(this).scrollTop() < 200 || hideOnClick == true) {
        $("#popUp").slideUp();
    } else {
        $("#popUp").slideDown();
    }
    if ($(this).scrollTop() == 0) {
        hideOnClick = false;
    }
});

$("#myClose").click(function(){
    $("#popUp").slideUp();
    hideOnClick = true;
});

/*
 *   sidebar fast rating questions element.html
 *
 *
 *
 */


$(document).ready(function() {
    $("button").click(function() {
        $("#fade-div-1").fadeTo("slow", 0.15);
        $("#fade-div-2").fadeTo("slow", 0.4);
        $("#div3").fadeTo("slow", 0.7);
    });


    isLargeWindow = $(this).width() > 768;

    $(window).on('resize', function() {
        isLargeWindow = $(this).width() > 768;
        if (sidebarVisible && isLargeWindow) {
            $("#wrapper").toggleClass("toggled");
            sidebarVisible = false;
        }
    });

});

var generalSustRatingDone = false;
var whenDone = false;
var typeDone = false;
var faded = true;

$("#generalSust").click(function() {
    generalSustRatingDone = true;

    if ((whenDone == true) && (typeDone == true) && (faded == true)) {
        unfadeQuestions();
    }

});


$("#datepicker").datepicker().on('changeDate', function() {
    whenDone = true;

    if ((generalSustRatingDone == true) && (typeDone == true) && (faded == true)) {
        unfadeQuestions();
    }

});

$("#id_when").change(function(){
    whenDone = true;

    if ((generalSustRatingDone == true) && (typeDone == true) && (faded == true)) {
        unfadeQuestions();
    }

});


$("#typeSelect").click(function() {
    typeDone = true;

    if ((generalSustRatingDone == true) && (whenDone == true) && (faded == true)) {
        unfadeQuestions();
    }

});


function unfadeQuestions() {
    faded = false;
    $(".sidebar-text").fadeTo("slow", 1);
}

/* end of sidebar fast rating questions */


/************************************************************************/


/**
 *   review.html rating questions
 *
 *   setting visibility for desktop version only
 *
 *
 */



// initially sidebar is hidden and no questions are loaded
var sidebarVisible = false;
var questionsDisplayed = "";




function loadQuestionsByCategory(sendData){
    // e.preventDefault();
    if (!sidebarVisible && isLargeWindow){
        $("#wrapper").toggleClass("toggled");
        sidebarVisible = true;
    }
     var promise = makeAjaxCall("/category-load-questions","GET","json",sendData)
      promise.done(function(data){
      var questions = JSON.parse(data.questions)
      if(questions.length>0){
         var questionsBoxParent= $("#sidebar-wrapper");
         var questionsBox;
        for(var counter=0; counter<questions.length; counter++){
          // do it only once 
          if(counter==0){
            var allQuestionsBox= $("#sidebar-wrapper div[class^='category_']");
            var excemptId = questions[counter].fields.rating_category;
            hideAlreadyRendered(allQuestionsBox,excemptId)
          }
           var categoryId = questions[counter].fields.rating_category;
               questionsBox = $("#sidebar-wrapper .category_"+categoryId);
            // undefined
            if (questionsBox.length<=0){
               questionsBox = $("<div class='category_"+categoryId+"' ></div>");
               questionsBox.attr("name","category_"+categoryId)
               questionsBoxParent.append(questionsBox)
            }
          renderRateableQuestion(questions[counter],questionsBox)
        }
        var loadModeButton = questionsBox.find(".load_more_q");
        if (loadModeButton.length<=0){
          loadModeButton = $("  <a class='load_more_q button-as-link'>Show me more</a>");
          questionsBox.append(loadModeButton);
        }
        questionsBox.css("display","block");
      }
  }); // end done

}// end method

// when category is clicked
$(".rate-sidebar").click(function(e){
    var data = {
    "page_number":1,
    "element_id": $("#reviewed_item").val(),
    "category_id":$(this).attr("name").split("_")[1]
    }
    // find panel accordion 
    var getParentDivPosition = $(this).parents(".panel-accordion").offset();
    // reposition sidebar wrapper
    $("#sidebar-wrapper").css({"top":(getParentDivPosition.top)-250});
    // load questions
loadQuestionsByCategory(data)

}); // end whole method


// load more questions for each category
$(document).on('click','.load_more_q',function(){

  varLoadedQuestions = $(this).siblings(".rating-question-item");
  pageNumber = (varLoadedQuestions.length)/3+1;

  if(Number.isInteger(pageNumber)){
    // hide already rendered
    var data = {
    "page_number":pageNumber,
    "element_id": $("#reviewed_item").val(),
    "category_id":$(this).parent().attr("name").split("_")[1]
    }
  hideAlreadyRendered(varLoadedQuestions);
  loadQuestionsByCategory(data)
  /* get button and delete it and create a new one
   to make sure it always stays at the end.
  */
  var loadModeButton = $(this).parent().find(".load_more_q").remove();
  loadModeButton = $("  <a class='load_more_q button-as-link'>Show me more</a>");
  $(this).parent().append(loadModeButton);
  }
});
// end


// send rated questions to django review form 
$("#review-form").on("submit", function(){

   var vistType =      $('#typeSelect input[name=type]:checked').val();
   var visitDate =     $('#datepicker input[name=when]').val();
   var generalRating = $('#general_rating .rating-input:checked').val();

    if (validateReviewForm(vistType,visitDate,generalRating)){
      // rated questions
     var ratedQuestionsList = getRatedQuestionsList();
     // stringify the data
     var ratedQuestions = JSON.stringify({"rated_questions":ratedQuestionsList})
    $("#id_json_data_helper").val(ratedQuestions);
  return true;
    }else{
      return false;
    }
});
 



// close sidebar in review form
$(".closing-sidebar").click(function(e) {
    if (sidebarVisible && isLargeWindow) {
        $("#wrapper").toggleClass("toggled");
        sidebarVisible = false;
    }
});


/* fast review form */
// validate fast review form
function validateReviewForm(vistType, visitDate, generalRating ){

  if (typeof vistType == 'undefined' || visitDate =="" || typeof generalRating == 'undefined' ){
         if(typeof vistType == 'undefined'){
          $('#typeSelect').find(".validation-text").addClass("show");
         }

          if(visitDate ==""){
         $('#datepicker').parent().find(".validation-text").addClass("show")
         }

          if(typeof generalRating == 'undefined'){
         $('.general-rating ').parent().find(".validation-text").addClass("show")
         }

         return false;
    }

    return true;
}

// fast review form
$('.general-rating  input[name=general_category]').on("change", function(){
$('.general-rating ').parent().find(".validation-text").removeClass("show")
});

// main review form
$('#general_rating  input[type=radio]').on("change", function(){
$('#general_rating ').parent().find(".validation-text").removeClass("show")
});

// main review form
$('#typeSelect input[name=type]').on("change", function(){
$('#typeSelect').find(".validation-text").removeClass("show")
});
// fast review form
$('#typeSelect input[name=radio]').on("change", function(){
$('#typeSelect').find(".validation-text").removeClass("show")
});

$("#datepicker").datepicker().on('changeDate', function(){
$('#datepicker').parent().find(".validation-text").removeClass("show")
});


function createRatedGlobes(ratingValue, ratingTree){
  for ( var fullGlobes=0; fullGlobes<ratingValue; fullGlobes++){
    ratingTree.append("<li style='float:left; padding:1px;'><i class='rating-on'></i></li>")
    }
  if (ratingValue<5){
   var unRated = 5-ratingValue;
  for(var emptyGlobes=0; emptyGlobes<unRated; emptyGlobes++){
      ratingTree.append("<li style='float:left;  padding:1px;' ><i class='rating-off'></i></li>")
  }
  }
}

// fast review form confirmation
function fastReviewConfirm(data){
  generalRating = data.categories_tree.mainCategory.rating;
  var generalRatingTree = $("#general-rating-tree");
            generalRatingTree.html("");
            // create for general
  createRatedGlobes(generalRating, generalRatingTree);

  var questions = $(".rating-question-item");


// append the rated categories
  var ratedCategories = $('.rating-category-item .rating .rating-input:checked');

  ratedCategories.each(function(index,element){

    var categoryRatingTree = $("<ul class='treeview col-md-12 col-ms-12 col-xs-12'> </ul>");
    // append the text
    $(".questions-confirm-box").append("<p>"+$(this).parent().siblings("p").html()+"</p>")
    // create question rating tree
    $(".questions-confirm-box").append(categoryRatingTree);
    // create rating globes
    createRatedGlobes($(this).val(), categoryRatingTree)

  });

// append the rated questions
  var ratedQuestions = $('.rating-question-item .rating .rating-input:checked');
  ratedQuestions.each(function(index, element){

    var questionRatingTree = $("<ul class='treeview col-md-12 col-ms-12 col-xs-12'> </ul>");
    // append the text
    $(".questions-confirm-box").append("<p>"+$(this).parent().siblings("p").html()+"</p>")
    // create question rating tree
    $(".questions-confirm-box").append(questionRatingTree);
    // create rating globes
    createRatedGlobes($(this).val(), questionRatingTree)
  });
}


function getRatedQuestionsList(){
   var ratedQuestions = $('.rating-question-item .rating .rating-input:checked');
     var ratedQuestionsList = [];
     if (ratedQuestions.length>0){
     ratedQuestions.each(function(index, element){
       // get id and rating value
       var ratedQuestionId = $(this).attr("name").split("_");
       ratedQuestionsList.push({"id":ratedQuestionId = ratedQuestionId[1],"value": $(this).val() });
     }); }

     return ratedQuestionsList
}


function makeAjaxCall(url,type,dataType,data){

     $.ajaxSetup({
         beforeSend: function(xhr, settings){
             if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                 xhr.setRequestHeader("X-CSRFToken", csrftoken);
             }
         }
     });

// return data
    return $.ajax({url: url,type: type,dataType: dataType,data:data});
}


 $("#fast_review_submit").on("click",function(event){
          event.preventDefault();
   var vistType = $('#typeSelect input[name=radio]:checked').val();
   var visitDate = $('#datepicker input[name=when]').val();
   var generalRating = $('.general-rating .rating-input:checked').val();
   var formIsValid = validateReviewForm(vistType, visitDate, generalRating)

   if (formIsValid){

    // rated questions
    var ratedQuestionsList = getRatedQuestionsList();

     // rated categories except general
     var ratedCategories =  $('.rating-category-item .rating .rating-input:checked');
     var ratedCategoriesList = [];

      if (ratedCategories.length>0){
     ratedCategories.each(function(index, element){
       // get id and rating value
       var ratedCategoryId = $(this).attr("name").split("_");
       ratedCategoriesList.push({"id":ratedCategoryId = ratedCategoryId[1],"value": $(this).val() });
     }); }

     // stringify the data
     var sendData = JSON.stringify({"element_id": $("#current_place_id").val(),
      "rating_value": generalRating,'type': vistType ,
      "rated_categories": ratedCategoriesList,
      "rated_questions":ratedQuestionsList, "date": visitDate })
    // make ajax call
    var promise= makeAjaxCall('/fast-review', 'POST','json',{"json_data":sendData})
    promise.done(function(data){
    fastReviewConfirm(data);
    $('#fast_review_confirm').modal('show');
    });
  }
});


$('#fast_review_confirm').on('hidden.bs.modal', function () {
  window.location.reload(true);
});

// load the first set of questions on document load
loadQuestions();
function loadQuestions(questionsPageNumber,categoriesPageNumber){

  console.log(categoriesPageNumber)

// load the first page on document load
  if (questionsPageNumber === undefined){
      questionsPageNumber = 1;
  }

  if (categoriesPageNumber === undefined){
    categoriesPageNumber= 1;
  }

  var pageNumber = 1;

  if((Number.isInteger(categoriesPageNumber)) && categoriesPageNumber >= questionsPageNumber){
    pageNumber = categoriesPageNumber
  }else if ((Number.isInteger(questionsPageNumber)) && questionsPageNumber >= questionsPageNumber){
    pageNumber = questionsPageNumber
  }else{
    console.log("no more questions or categories to load");
    pageNumber = "end of the list";
  }

// check if we have reached at the end of the questions
if(Number.isInteger(pageNumber)){

    sendData = {'page_number': pageNumber, 'element_id': $("#current_place_id").val() , 'csrfmiddlewaretoken': $( "#csrfmiddlewaretoken" ).val()}
    var promise= makeAjaxCall('/load-question', 'GET','json',sendData)
    promise.done(function(data){
        // get rating questions
        questions = JSON.parse(data.questions)
        var currentQuestions = $(".rating-questions-box .rating-question-item");
            // hide already rated or current questions
            hideAlreadyRendered(currentQuestions);
              // render loaded questions
         var questionsBox= $("#rating_questions_box");
        for (var counter =0; counter< questions.length; counter++ ){
              renderRateableQuestion(questions[counter],questionsBox);
          }

        // get rating categories 
        ratingCategories = data.rating_categories;
        // hide already rated categories 
        var currentCategories = $(".rating-questions-box .rating-category-item");
            hideAlreadyRendered(currentCategories);
             // render loaded rating categories 
        for (var counter=0; counter<ratingCategories.length; counter++){
          renderRatingCategory(ratingCategories[counter]);
        }
    });


}else{
  console.log("sorry, no more questions to load");
  // hide the button
  $("#load_more_questions").addClass("hide");
}}


function hideAlreadyRendered(renderObjects){
  if(renderObjects.length>0){
          renderObjects.each(function(){
            $(this).css("display","none");
          })
}}


$("#load_more_questions").on("click", function(){
    event.preventDefault();
 var loadedQuestions =  $(".rating-question-item");
 var loadedCategories = $(".rating-category-item");
 var questionsPageNumber = (loadedQuestions.length/2)+1;
 var categoriesPageNumber = (loadedCategories.length/2)+1;

 loadQuestions(questionsPageNumber,categoriesPageNumber);
})


function renderRatingCategory(category){// start method
        var questionsBox= $("#rating_questions_box");

        var isCategoryLoaded= $("#categoryitem_"+category.pk);

        if (isCategoryLoaded.length== 0){
          var categoryItem = $("<div id='categoryitem_"+category.pk+"' class='rating-category-item row'></div>")
          var categoryTextBox = $("<p class='sidebar-text'>"+category.name+"</p>")
            categoryItem.append(categoryTextBox);
            questionsBox.append(categoryItem);
          var ratingBox = $("<div class='fieldWrapper rating' data-toggle='tooltip'></div>");
                ratingBox.attr('id','rat_cat_'+category.pk);
                ratingBox.attr('data-placement','top');
                ratingBox.attr('title',category.help_text);
                // bind the tooltip
                ratingBox.tooltip();
            // create the rating globes
            for (var globes =0; globes<5; globes++){
                          // starts from 5
                 var globeInput=$("<input name='ratingcategory_"+category.pk+"' type='radio' class='rating-input'>");
                      globeInput_id = "rating_category_"+category.pk+"_"+globes
                      globeInput.attr("id",globeInput_id);
                      globeInput.attr("value",(5-globes));
                var globeLabel = $("<label class='rating-star'></label>");
                    globeLabel.attr("for",globeInput_id);
                     ratingBox.append(globeInput)
                     ratingBox.append(globeLabel)
            }

            var clearButton = generalClearButton();

            // append rating box
            categoryItem.append(ratingBox);
            categoryItem.append(clearButton);
          
     }
  }// end method


function renderRateableQuestion(question, questionsBox){// start method
        var isQuestionLoaded= $("#questionitem_"+question.pk);
        if (isQuestionLoaded.length== 0){
          var questionItem = $("<div id='questionitem_"+question.pk+"' class='rating-question-item row'></div>");
          var questionTextBox = $("<p class='sidebar-text'>"+question.fields.text+"</p>")
            questionItem.append(questionTextBox);
            questionsBox.append(questionItem);
          var ratingBox = $("<div class='fieldWrapper rating'></div>");
            // create the rating globes
            for (var globes =0; globes<5; globes++){
                          // starts from 5
                 var globeInput=$("<input name='ratingquestion_"+question.pk+"' type='radio' class='rating-input'>");
                      globeInput_id = "rating_question_"+question.pk+"_"+globes
                      globeInput.attr("id",globeInput_id);
                      globeInput.attr("value",(5-globes));
                var globeLabel = $("<label class='rating-star'></label>");
                    globeLabel.attr("for",globeInput_id);
                     ratingBox.append(globeInput)
                     ratingBox.append(globeLabel)
            }
            var clearButton = generalClearButton();
            questionItem.append(ratingBox);
            questionItem.append(clearButton);
     }
  }// end method


function generalClearButton(){

   var clearButton= $("<button><i class='fa fa-times' aria-hidden='true'></i></button>");
                                    clearButton.attr("data-toggle","tooltip");
                                    clearButton.attr("data-placement","top");
                                    clearButton.attr("title","Reset your rating");
                                    clearButton.css({"display":"none"});
                                    clearButton.addClass( "button-as-link clear-button pull-right");
                                    clearButton.tooltip();
   return clearButton;
}

function renderRatedQuestions(ratedQuestions, contentDropDown){

   // if this review has any questions
      if (ratedQuestions.length>0){
          for (var counter =0; counter<ratedQuestions.length; counter++){
           var elRow = $("<div class='row'> </div>");
           var elTextContainer = $("<div class='sidebar-text-dropdown'></div>");
           var elQuestionP = $("<p>"+ratedQuestions[counter].questionText+"</p>");
           elTextContainer.append(elQuestionP);
           elRow.append(elTextContainer);
           var elRatingResult = $("<div class='col-xs-12 rating-result'> </div>");
           var elRatingGlobes = $("<div class='rating-globes-dropdown'></div>")
           var questionRatingTree = $("<ul class='treeview col-md-12 col-ms-12 col-xs-12'> </ul>");
           createRatedGlobes(ratedQuestions[counter].value, questionRatingTree)
           elRatingGlobes.append(questionRatingTree);
           elRatingResult.append(elRatingGlobes);
           elRow.append(elRatingResult);
            // append the row of question
           contentDropDown.append(elRow);
            // append a divider after each row of question
           contentDropDown.append("<div class='divider'></div>");
          }
          contentDropDown = element.next(".myDropdown").toggleClass("show");
      }
}


// load rated questions for every category (mode and rated question)
$(".load-rated-questions").on("click", function load_rated_questions(){
  element = $(this);
   sendData = {'element_id': $("#current_place_id").val(), "category_id":element.attr("id")}
   var promise= makeAjaxCall('/load-rated-question', 'GET','json',sendData)
   promise.done(function(data){
     var ratedQuestions = data.ratedQuestions;
          var elContentDropDown = element.next(".myDropdown");
          if (elContentDropDown.length ==0 ){
             elContentDropDown = $("<div style='position:relative' class='dropdown-content myDropdown'></div>");
             element.parent().append(elContentDropDown)
         }
           elContentDropDown.html("");
           renderRatedQuestions(ratedQuestions, elContentDropDown);
           element.parents(".cat-panel-accordion").css("max-height","250px");
   });
});

$(document).on('click','.remove-questions-box',function(){
  $(this).parents(".questions-wrapper").remove();
});

/* displaying ratedQuestions for reviews */
$(document).on('click','.display-review-questions',function(){
   element = $(this);
    sendData = {'element_id': $("#current_place_id").val(), "review_id":element.attr("id")}
   var promise= makeAjaxCall('/rated-questions-reviews', 'GET','json',sendData)
   promise.done(function(data){
     var reviewQuestions = data.reviewQuestions
       var elContentDropDown = element.next(".myDropdown");
          if (elContentDropDown.length ==0 ){
             elContentDropDown = $("<div style='position:absolute' class='dropdown-content myDropdown'></div>");
             element.parent().append(elContentDropDown)
         }
          // empty incase of repeated button presses
         elContentDropDown.html("");
      renderRatedQuestions(reviewQuestions, elContentDropDown);
   });
});


$(document).on("click",'#preview_ratings',function(event){

event.preventDefault();

   var vistType = $('#typeSelect input[name=radio]:checked').val();
   var visitDate = $('#datepicker input[name=when]').val();
   var generalRating = $('.general-rating .rating-input:checked').val();
   var formIsValid = validateReviewForm(vistType, visitDate, generalRating)

   if(formIsValid){

    console.log(" I was here");


     var ratedQuestions = $('.rating-question-item .rating .rating-input:checked');
      var currentQuestions = $(".rating-questions-box .rating-question-item");

       if (ratedQuestions.length>0){

            var elRow = $("<div class ='row questions-wrapper'></div>");
            var headerText = $("<div class='rated-questions-header text-center'><h4>Preview your ratings</h4><button class='remove-questions-box'>X</button><div>");
            var ratedQuestionsBox = $("<div class ='col-md-12 category-rated-questions-box' ></div> ");
            var ratedQuestionsContent = $("<div class='col-md-12 rated-questions-content'></div>")
            ratedQuestionsBox.append(headerText);
            ratedQuestionsBox.append(ratedQuestionsContent);
            elRow.append(ratedQuestionsBox);
              $("#main").append(elRow);


               ratedQuestions.each(function(){

                console.log($(this));
                var questionLabel = $(this).next("label").html();
                var selectedValue = $(this).val();
                

                console.log(questionLabel);
                console.log(selectedValue);

               });
          }
   }
});

// Close the dropdown if the user clicks outside of it
window.onclick = function(event){
  if (!event.target.matches('.dropDownIcon')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')){

        openDropdown.classList.remove('show');
        openDropdown.remove();
      }
    }
  }
}

//  csrf and cookies 
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
    // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method){
 // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

// end iife
}());


/**
 *   end of review.html rating questions
 *
 *
 *
 *
 */
