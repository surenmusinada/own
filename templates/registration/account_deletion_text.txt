{% load i18n %}
{% load tags %}

We are sorry to see you go. It's been great having you as a sustainability rater on One Planet Rating.
Please let us know why you have decided to leave so we can improve the service.

1. The service did not match my expectations.
2. The service was difficult to use and understand.
3. Other, please specify.

Sincerely,

The One Planet Rating team